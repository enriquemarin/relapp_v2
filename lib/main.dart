import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:relapp_pruebas/services/auth_service.dart';
import 'package:relapp_pruebas/view/auth_screen.dart';
//import 'package:relapp_pruebas/providers/notifications_push_provider.dart';
//import 'package:relapp_pruebas/view/notification_content.dart';

void main() => runApp(MyApp());

setPortraitOrientation() async {
  await SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
}

class MyApp extends StatefulWidget {
  MyApp({Key key}) : super(key: key);

  @override
  MyAppState createState() => MyAppState();
}

class MyAppState extends State<MyApp> {
 // final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  @override
  void initState() {
    super.initState();
   /*  final pushProvider = NotificationPushProvider();
    pushProvider.initNotifications();

    pushProvider.notifications.listen((noti) {
      navigatorKey.currentState.pushNamed('Noti', arguments: noti);
    }); */
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      systemNavigationBarColor: Colors.white,
      systemNavigationBarIconBrightness: Brightness.dark,
      statusBarColor: Color(0x00FFFFFF),
      statusBarIconBrightness: Brightness.dark,
    ));

    return MaterialApp(
      title: 'Rel',
      //navigatorKey: navigatorKey,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // Define the default Brightness and Colors
        brightness: Brightness.light,
        primaryColor: Color.fromARGB(255, 255, 255, 255),
        accentColor: Colors.cyan[600],
        backgroundColor: Color.fromARGB(0, 255, 255, 255),
        // Define the default Font Family
        //
        fontFamily: 'Gilroy',

        textTheme: TextTheme(
          headline: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
          title: TextStyle(
            fontSize: 24.0,
            fontStyle: FontStyle.normal,
          ),
          body1: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
        ),
        inputDecorationTheme: InputDecorationTheme(
          alignLabelWithHint: false,
          border: InputBorder.none,
          hintStyle: TextStyle(
            fontFamily: 'Gilroy',
            fontSize: 24.0,
            fontStyle: FontStyle.normal,
          ),
        ),
        platform: TargetPlatform.iOS,
      ),
      home: AuthScreen(auth: Auth()));
  }
}
