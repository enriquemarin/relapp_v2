import 'package:flutter/material.dart';
import 'package:relapp_pruebas/plugins/widgets.dart';
import 'package:relapp_pruebas/model/model_categoria.dart';
import 'package:relapp_pruebas/model/model_company.dart';
import 'package:relapp_pruebas/services/category_service.dart';
import 'package:relapp_pruebas/services/company_service.dart';

class Categoria extends StatefulWidget {
  final CategoriaModel category;
  Categoria({Key key, this.category}) : super(key: key);

  @override
  _CategoriaState createState() => _CategoriaState();
}

class _CategoriaState extends State<Categoria> {
  CategoriaModel _categoria;
  List<CompanyModel> _companyList;
  final CategoryService _categoryService = CategoryService();
  final CompanyService _companyService = CompanyService();

  getCurrentCategory() async {
    _categoria = await _categoryService.getCategoryByName(widget.category);
  }

  getCompanyList() async {
    _companyList = await _companyService.getCompanies();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: Colors.white),
      child: _builSubcategoryList(),
    );
  }

  @override
  void initState() {
    super.initState();
    getCompanyList();
  }

  Widget _builSubcategoryList() {
    return FutureBuilder(
        future: getCurrentCategory(),
        builder: (_, snapshot) {
          if (snapshot.connectionState == ConnectionState.done &&
              snapshot.hasData) {
            return Container(child: _tabBar(_categoria));
          } else {
            return Center(child: CircularProgressIndicator());
          }
        });
  }

  Widget _tabBar(CategoriaModel category) {
    TabController _tabController;

    return Scaffold(
      body: DefaultTabController(
        length: category.subcategorias.length,
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            backgroundColor: Colors.white,
            automaticallyImplyLeading: true,
            elevation: 2.0,
            title: Text(
              category.nombreCategoria,
              style: TextStyle(
                color: category.colordefondo,
                fontSize: 22,
                fontFamily: 'GilroyExtraBold',
              ),
            ),
            bottom: TabBar(
                labelColor: Color(0xFF4B4B4B),
                unselectedLabelColor: Color(0xFF4B4B4B),
                isScrollable: true,
                indicatorWeight: 5.0,
                indicatorColor: category.colordefondo,
                tabs: category.subcategorias.map((index) {
                  return Tab(text: index);
                }).toList()),
          ),
          body: TabBarView(
            controller: _tabController,
            children: category.subcategorias.map((index) {
              return _builCompanyList(category.nombreCategoria, index);
            }).toList(),
          ),
        ),
      ),
    );
  }

  Widget _builCompanyList(String category, String subcategory) {
    var filterCompanyList;

    if (subcategory == 'Todos') {
      filterCompanyList =
          _companyList.where((c) => c.category == category).toList();
    } else {
      filterCompanyList = _companyList
          .where((c) => c.category == category && c.subcategory == subcategory)
          .toList();
    }

    return Container(
      child: Padding(
        padding: const EdgeInsets.all(0.0),
        child: ListView.builder(
          scrollDirection: Axis.vertical,
          itemCount: filterCompanyList.length,
          itemBuilder: (_, index) {
            CompanyModel company = filterCompanyList[index];
            return CustomCompanyCard(company: company);
          },
        ),
      ),
    );
  }
}
