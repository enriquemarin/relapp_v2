import 'dart:async';
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:relapp_pruebas/model/model_categoria.dart';
import 'package:relapp_pruebas/model/model_company.dart';
import 'package:relapp_pruebas/model/model_sucursal.dart';
import 'package:relapp_pruebas/plugins/widgets.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:relapp_pruebas/plugins/show_selected_pin.dart' as pin;

class Mapa extends StatefulWidget {
  Mapa({Key key}) : super(key: key);

  @override
  _MapaState createState() => _MapaState();
}

class _MapaState extends State<Mapa> with TickerProviderStateMixin {
  TabController _tabLocalController;
  List<CompanyModel> _companyListResult;

  Location location = new Location();
  GoogleMapController _mapController;
//  Marker marker;
  Position _userLocation;
  double _latitud = 0;
  double _longitud = 0;
  var buscarDir;
  bool _mostrarCard = false;
  var _companyDetailVisible = false;
  var _cardDetailHeight = 0.0;
  var _cardDetailHeightmin = 100.0;
  var _cardDetailHeightmax = 500.0;
  var _tabAreaHeight = 0.0;
  var _tabAreaHeightMax = 300.0;
  var _tabAreaHeightMin = 0.0;

  CameraPosition _initialPosition = CameraPosition(
    target: LatLng(24.153512, -110.309349),
    zoom: 15.0,
  );

  List<Marker> _marcadores = [];

  CompanyModel _currentCompany = CompanyModel();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabLocalController = TabController(length: 3, vsync: this);
    _getAllCompanies();
    _getLocation();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          GoogleMap(
            onMapCreated: onMapCreated,
            initialCameraPosition: _initialPosition,
            myLocationEnabled: false,
            mapType: MapType.normal,
            markers: Set<Marker>.of(_marcadores),
            compassEnabled: false,
          ),
          Positioned(
            top: 46.0,
            right: 16.0,
            left: 16.0,
            child: Container(
              height: 82.0,
              width: double.infinity,
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: _buildCategorias(),
                  ),
                ],
              ),
            ),
          ),
          mostrarInfo(),
        ],
      ),
    );
  }

  //WIDGETS

  Widget mostrarInfo() {
    if (_currentCompany.companyId != null) {
      return Align(
        alignment: Alignment.bottomCenter,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            AnimatedContainer(
              duration: Duration(milliseconds: 120),
              height: _cardDetailHeight,
              width: double.infinity,
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.black45,
                    blurRadius: 20.0,
                    offset: Offset(
                      0.0,
                      10.0,
                    ),
                  ),
                ],
                borderRadius: BorderRadius.only(
                  topLeft: const Radius.circular(36.0),
                  topRight: const Radius.circular(36.0),
                ),
                color: Colors.white,
              ),
              child: Visibility(
                visible: _mostrarCard,
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 24.0, right: 24.0, top: 16.0),
                    child: Column(
                      children: <Widget>[
                        InkWell(
                          onTap: () {
                            setState(() {
                              if (_cardDetailHeight == _cardDetailHeightmax) {
                                _tabAreaHeight = _tabAreaHeightMin;
                                _cardDetailHeight = _cardDetailHeightmin;
                                //mostrarCard = false;
                              } else {
                                _cardDetailHeight = _cardDetailHeightmax;

                                Timer(Duration(milliseconds: 500), () {
                                  setState(() {
                                    _tabAreaHeight = _tabAreaHeightMax;
                                  });
                                });
                                // mostrarCard = true;
                              }
                            });
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(
                                left: 4.0, right: 4.0, top: 2.0, bottom: 2.0),
                            child: Container(
                              height: 6,
                              width: 30,
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(36.0)),
                                color: Colors.black,
                              ),
                            ),
                          ),
                        ),
                        Container(
                          child: Column(
                            children: <Widget>[
                              SizedBox(height: 5.0),
                              Visibility(
                                visible: _companyDetailVisible,
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      height: 60.0,
                                      width: 60.0,
                                      child: CircleAvatar(
                                        backgroundImage: NetworkImage(
                                            _currentCompany.profileImage),
                                      ),
                                    ),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 20.0, bottom: 4.0),
                                            child: Text(_currentCompany.name,
                                                style: TextStyle(
                                                    fontSize: 16.0,
                                                    fontWeight: FontWeight.bold,
                                                    color: Color(0xFF4B4B4B))),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 20.0, bottom: 12.0),
                                            child: Wrap(
                                              children: _currentCompany.tags
                                                  .map((tag) {
                                                return Text(
                                                  tag + ' ',
                                                  textAlign: TextAlign.left,
                                                  style: TextStyle(
                                                    color: Color(0xFF4B4B4B),
                                                  ),
                                                );
                                              }).toList(),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              _tabArea(),
                              /*  Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                              PurpleButton(text: 'Ver perfil', onTap: (){}),
                              PurpleButton(text: 'Ruta', onTap: (){}),
                            ],), */
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return Container();
    }
  }

  Widget _buildCategorias() {
    return ListView.builder(
        scrollDirection: Axis.horizontal,
        padding:
            const EdgeInsets.only(left: 16.0, right: 16.0, top: 8, bottom: 8),
        itemCount: categoriasForSearchMapLista.length,
        itemBuilder: (_, index) {
          return CustomCircleMiniCategory(
            categoriaModel: categoriasForSearchMapLista[index],
            onTap: () {
              _cardDetailHeight = 0;
              _mostrarCard = false;
              _setMarkers(categoriasForSearchMapLista[index].nombreCategoria);
            },
          );
        });
  }

  Widget _tabArea() {
    double screenHeight = MediaQuery.of(context).size.height;
    return Container(
      height: _tabAreaHeight,
      child: Padding(
        padding: const EdgeInsets.only(top: 32.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            TabBar(
              controller: _tabLocalController,
              indicator: BubbleTabIndicator(
                indicatorHeight: 25.0,
                indicatorColor: Color(0XFFEAEAEA),
                tabBarIndicatorSize: TabBarIndicatorSize.tab,
              ),
              indicatorSize: TabBarIndicatorSize.tab,
              indicatorColor: Color(0xFF4B4B4B),
              labelColor: Color(0xFF4B4B4B),
              unselectedLabelColor: Colors.black54,
              isScrollable: true,
              tabs: <Widget>[
                Tab(
                  child: TabText(text: 'Horarios'),
                ),
                _textoParaUbicaciones(_currentCompany.isCompany),
                Tab(
                  child: TabText(text: 'Contacto'),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.all(5.0),
            ),
            Container(
              height: screenHeight * 0.30,
              margin: EdgeInsets.only(left: 16.0, right: 16.0),
              child: TabBarView(
                physics: NeverScrollableScrollPhysics(),
                controller: _tabLocalController,
                children: <Widget>[
                  _buildSchedule(_currentCompany),
                  _buildSurcursalList(_currentCompany),
                  Container(
                    child: Column(
                      children: <Widget>[
                        _phoneCompany(_currentCompany),
                        _emailCompany(_currentCompany),
                        _webLinkCompany(_currentCompany)
                      ],
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildSchedule(CompanyModel company) {
    if (company.schedule != null) {
      var scheduleList = company.schedule.values.toList();
      var days = [
        'Lunes',
        'Martes',
        'Miércoles',
        'Jueves',
        'Viernes',
        'Sábado',
        'Domingo'
      ];
      return ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        itemCount: scheduleList.length,
        itemBuilder: (_, index) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 32.0),
            child: Padding(
              padding: const EdgeInsets.only(bottom: 5.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  GeneralText(text: days[index]),
                  GeneralText(text: scheduleList[index]),
                ],
              ),
            ),
          );
        },
      );
    } else {
      return Container();
    }
  }

  Widget _textoParaUbicaciones(bool isCompany) {
    if (isCompany == false) {
      return Align(
        alignment: Alignment.centerLeft,
        child: TabText(text: 'Ubicacion'),
      );
    } else {
      return Align(
        alignment: Alignment.centerLeft,
        child: TabText(text: 'Sucursales'),
      );
    }
  }

  Widget _buildSurcursalList(CompanyModel company) {
    List<DocumentSnapshot> documents;
    var sucursalSize = 85.0;

    Future getSucursales() async {
      var firestore = Firestore.instance;
      QuerySnapshot q = await firestore
          .collection('company')
          .document(company.companyId)
          .collection('sucursal')
          .getDocuments();
      documents = q.documents;
      return q.documents;
    }

    return FutureBuilder(
        future: getSucursales(),
        builder: (_, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            return Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Container(
                height: sucursalSize * documents.length,
                child: ListView.builder(
                  padding: EdgeInsets.only(top: 16.0, bottom: 16.0),
                  //physics: NeverScrollableScrollPhysics(),
                  itemCount: documents.length,
                  itemBuilder: (_, index) {
                    SucursalModel sucursal = new SucursalModel();
                    //populate object
                    if (snapshot.data[index].data['sucursalId'].toString() ==
                        'none') {
                      sucursal.address = company.address;
                      sucursal.facebookLink = company.facebookLink;
                      sucursal.mapLink = company.mapLink;
                      sucursal.name = company.name;
                      sucursal.phone = company.phone;
                      sucursal.sucursalId = company.companyId;
                    } else {
                      sucursal.address =
                          snapshot.data[index].data['address'].toString();
                      sucursal.facebookLink =
                          snapshot.data[index].data['facebookLink'].toString();
                      sucursal.mapLink =
                          snapshot.data[index].data['mapLink'].toString();
                      sucursal.name =
                          snapshot.data[index].data['name'].toString();
                      sucursal.phone =
                          snapshot.data[index].data['phone'].toString();
                      sucursal.sucursalId =
                          snapshot.data[index].data['sucursalId'].toString();
                    }

                    return Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        _itemCardSucursal(sucursal),
                      ],
                    );
                  },
                ),
              ),
            );
          }
        });
  }

  Widget _itemCardSucursal(SucursalModel sucursal) {
    _openUrl(url) async {
      if (await canLaunch(url)) {
        await launch(url);
      } else {
        //throw 'Error no se puede abrir';
      }
    }

    return Padding(
      padding: const EdgeInsets.only(bottom: 20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          InkWell(
            onTap: () {
              _openUrl(sucursal.mapLink);
            },
            child: Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Image(
                    height: 20.0,
                    image: AssetImage('assets/locationicongray.png'),
                  ),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        sucursal.name,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color(0xFF4B4B4B)),
                      ),
                      Text(sucursal.address),
                    ],
                  ),
                ),
              ],
            ),
          ),
          InkWell(
            child: Padding(
              padding: const EdgeInsets.only(left: 30.0, top: 5.0),
              child: Text(
                sucursal.phone,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Color(0xFF3D82D3),
                  fontSize: 14.0,
                ),
              ),
            ),
            onTap: () {
              _openUrl('tel://' + sucursal.phone);
            },
          ),
        ],
      ),
    );
  }

  Widget _phoneCompany(CompanyModel company) {
    double tamano = MediaQuery.of(context).size.width - 60;
    if (company.phone == '' || company.phone == null) {
      return Center();
    } else {
      return Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                  left: 8.0, right: 0.0, top: 8, bottom: 8.0),
              child: Container(
                height: 20.0,
                width: 20.0,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/phoneicongray.png'),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ),
            ConstrainedBox(
              constraints:
                  BoxConstraints(maxHeight: 30, maxWidth: tamano, minWidth: 5),
              child: MaterialButton(
                onPressed: () {
                  _openUrl('tel://' + company.phone);
                },
                child: Text(
                  company.phone,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF3D82D3),
                    fontSize: 16.0,
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    }
  }

  Widget _emailCompany(CompanyModel company) {
    double tamano = MediaQuery.of(context).size.width - 60;
    if (company.email == '' || company.email == null) {
      return Center();
    } else {
      return Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                  left: 8.0, right: 0.0, top: 8, bottom: 8.0),
              child: Container(
                height: 20.0,
                width: 20.0,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/email.png'),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ),
            ConstrainedBox(
              constraints:
                  BoxConstraints(maxHeight: 30, maxWidth: tamano, minWidth: 5),
              child: MaterialButton(
                onPressed: () {
                  _sendMail(company.email, '', '');
                },
                child: Text(
                  company.email,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF3D82D3),
                    fontSize: 16.0,
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    }
  }

  Widget _webLinkCompany(CompanyModel company) {
    double tamano = MediaQuery.of(context).size.width - 60;
    if (company.webLink == '' || company.webLink == null) {
      return Center();
    } else {
      return Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                  left: 8.0, right: 0.0, top: 8, bottom: 8.0),
              child: Container(
                height: 20.0,
                width: 20.0,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/language.png'),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ),
            ConstrainedBox(
              constraints:
                  BoxConstraints(maxHeight: 30, maxWidth: tamano, minWidth: 5),
              child: MaterialButton(
                onPressed: () {
                  _openUrl(company.webLink);
                },
                child: Text(
                  company.webLink,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF3D82D3),
                    fontSize: 16.0,
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    }
  }
//METODOS

  _setMarkers(String categoria) {
    setState(() {
      var filterList = _companyListResult
          .where((c) =>
              c.category.toLowerCase() == categoria.toString().toLowerCase())
          .toList();
      _marcadores.clear();
      for (var item in filterList) {
        if (item.geopoint != null) {
          double _lat = item.geopoint['lat'];
          double _long = item.geopoint['long'];
          _marcadores.add(
            Marker(
              markerId: MarkerId(item.companyId),
              draggable: false,
              consumeTapEvents: true,
              icon: pin.ShowSelectedPin().showPin(item.category),
              position: LatLng(_lat.toDouble(), _long.toDouble()),
              onTap: () {
                setState(() {
                  /*   if (height == heightmin) {
                    height = heightmax;
                    mostrarCard = false;
                  } else {
                    height = heightmin;
                    Timer(Duration(milliseconds: 120), () {
                      setState(() {
                        mostrarCard = true;
                      });
                    });
                    moverCamara(CameraPosition(
                      target: LatLng(_lat.toDouble(), _long.toDouble()),
                      zoom: 18.0,
                     // tilt: 50.0,
                     // bearing: 45.0,
                    ));
                    _currentCompany = item;
                  } */
                  Timer(Duration(milliseconds: 130), () {
                    setState(() {
                      _tabAreaHeight = _tabAreaHeightMin;
                      _cardDetailHeight = _cardDetailHeightmin;
                      _mostrarCard = true;
                      Timer(Duration(milliseconds: 500), () {
                        setState(() {
                          _companyDetailVisible = true;
                        });
                      });
                    });
                  });
                  moverCamara(CameraPosition(
                    target: LatLng(_lat.toDouble(), _long.toDouble()),
                    zoom: 18.0,
                    // tilt: 50.0,
                    // bearing: 45.0,
                  ));
                  _currentCompany = item;
                });
              },
              infoWindow: InfoWindow(title: "Hola", snippet: "como estás"),
            ),
          );
        }
      }
      moverCamara(CameraPosition(
        target: LatLng(24.143053, -110.314402),
        zoom: 13.5,
        //tilt: 50.0,
        //bearing: 45.0,
      ));
    });
  }

  Future _getAllCompanies() async {
    var companies = new List<CompanyModel>();
    _companyListResult = new List<CompanyModel>();
    QuerySnapshot getCompanies =
        await Firestore.instance.collection('company').getDocuments();

    for (var item in getCompanies.documents) {
      companies.add(new CompanyModel(
          companyId: item.data['companyId'].toString(),
          name: item.data['name'].toString(),
          description: item.data['description'].toString(),
          email: item.data['email'].toString(),
          category: item.data['category'].toString(),
          address: item.data['address'].toString(),
          subcategory: item.data['subcategory'].toString(),
          phone: item.data['phone'].toString(),
          facebookLink: item.data['facebookLink'].toString(),
          mapLink: item.data['mapLink'].toString(),
          webLink: item.data['webLink'].toString(),
          profileImage: item.data['profileImage'].toString(),
          covers: item.data['cover'],
          tags: item.data['tag'],
          schedule: item.data['schedule'],
          geopoint: item.data['geopoint'],
          isCompany: item.data['isCompany'],
          destacado: item.data['destacado']));
    }

    _companyListResult = companies;
  }

  void _getLocation() async {
    LocationData currentLocation;
    currentLocation = await location.getLocation();
    _latitud = currentLocation.latitude;
    _longitud = currentLocation.longitude;
    posicionActual();
  }

  posicionActual() {
    _initialPosition =
        CameraPosition(target: LatLng(_latitud, _longitud), zoom: 1.0);
    moverCamara(_initialPosition);
  }

  barraBusqueda() {
    Geolocator().placemarkFromAddress(buscarDir).then((result) {
      moverCamara(CameraPosition(
        target: LatLng(
          result[0].position.latitude,
          result[0].position.longitude,
        ),
        zoom: 10.0,
      ));
    });
  }

  void onMapCreated(GoogleMapController controller) {
    _getLocation();
    setState(() {
      _mapController = controller;
    });
  }

  void moverCamara(CameraPosition cameraPosition) {
    setState(() {
      if (_mapController != null) {
        _mapController
            .animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
      }
    });
  }

  _sendMail(String toMailId, String subject, String body) async {
    var url = 'mailto:$toMailId?subject=$subject&body=$body';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  _openUrl(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      //throw 'Error no se puede abrir';
    }
  }
}
