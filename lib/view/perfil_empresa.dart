import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:relapp_pruebas/model/model_company.dart';
import 'package:relapp_pruebas/model/model_promotion.dart';
import 'package:relapp_pruebas/model/model_sucursal.dart';
import 'package:relapp_pruebas/plugins/widgets.dart';
import 'package:relapp_pruebas/services/auth_service.dart';
import 'package:relapp_pruebas/services/rating_service.dart';
import 'package:url_launcher/url_launcher.dart';

class PerfilEmpresa extends StatefulWidget {
  final CompanyModel company;

  PerfilEmpresa({this.company});

  _PerfilEmpresaState createState() => _PerfilEmpresaState();
}

class _PerfilEmpresaState extends State<PerfilEmpresa>
    with TickerProviderStateMixin {
  bool correoElectronico = false;
  bool sitioWeb = false;
  TabController _tabController;
  final RatingService ratingService = RatingService();
  final Auth auth = Auth();
  double _companyRatingReview;
  String _userId;

  _sendMail(String toMailId, String subject, String body) async {
    var url = 'mailto:$toMailId?subject=$subject&body=$body';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  _openUrl(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      //throw 'Error no se puede abrir';
    }
  }

  Future _showImage(String image) async {
    return showDialog(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: const EdgeInsets.all(0),
          content: Container(
            child: Image(
              image: NetworkImage(image),
            ),
          ),
        );
      },
    );
  }

  Future _getCompanyRatingReview() async {
    _companyRatingReview = await ratingService.getCompanyReview(widget.company.companyId, _userId);
  }

  Future _calculateCompanyRating() async {
    await ratingService.caculateRatingByCompany(widget.company.companyId);
  }

  @override
  void initState() {
    super.initState();
    auth.getCurrentUser().then((user) {
      _userId = user.uid;
    });
    _calculateCompanyRating();
    _tabController = TabController(length: 3, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(0.0),
        child: SizedBox(
          height: 0.0,
        ),
      ),
      primary: true,
      body: _builCompanyDetail(),
    );
  }

  Widget _builCompanyDetail() {
    return ListView(
      children: <Widget>[
        _cover(context, widget.company.covers),
        Container(
          child: Padding(
            padding: const EdgeInsets.only(left: 16.0, right: 16.0, top: 24.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _companyDetail(widget.company),
                _tabArea(),
                SizedBox(
                  height: 24.0,
                ),
                SubtitleText(text: 'Califica este negocio'),
                SizedBox(
                  height: 6.0,
                ),
                _ratingReview(widget.company.companyId),
                SizedBox(
                  height: 24.0,
                ),
                SubtitleText(text: 'Promociones'),
                SizedBox(
                  height: 6.0,
                ),
                _buildPromotionList(widget.company),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _tabArea() {
    double screenHeight = MediaQuery.of(context).size.height;
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        TabBar(
          controller: _tabController,
          indicator: BubbleTabIndicator(
            indicatorHeight: 25.0,
            indicatorColor: Color(0XFFEAEAEA),
            tabBarIndicatorSize: TabBarIndicatorSize.tab,
          ),
          indicatorSize: TabBarIndicatorSize.tab,
          indicatorColor: Color(0xFF4B4B4B),
          labelColor: Color(0xFF4B4B4B),
          unselectedLabelColor: Colors.black54,
          isScrollable: true,
          tabs: <Widget>[
            Tab(
              child: TabText(text: 'Horarios'),
            ),
            _textoParaUbicaciones(widget.company.isCompany),
            Tab(
              child: TabText(text: 'Contacto'),
            ),
          ],
        ),
        Padding(
          padding: EdgeInsets.all(16.0),
        ),
        Container(
          height: screenHeight * 0.35,
          margin: EdgeInsets.only(left: 16.0, right: 16.0),
          child: TabBarView(
            physics: NeverScrollableScrollPhysics(),
            controller: _tabController,
            children: <Widget>[
              _buildSchedule(widget.company),
              _buildSurcursalList(widget.company),
              Container(
                child: Column(
                  children: <Widget>[
                    _phoneCompany(widget.company),
                    _emailCompany(widget.company),
                    _webLinkCompany(widget.company)
                  ],
                ),
              ),
            ],
          ),
        )
      ],
    );
  }

  //Widget para el cover
  Widget _cover(BuildContext context, List<dynamic> covers) {
    return SizedBox(
      height: 250.0,
      width: MediaQuery.of(context).size.width,
      child: Carousel(
        boxFit: BoxFit.cover,
        autoplay: false,
        animationCurve: Curves.fastOutSlowIn,
        animationDuration: Duration(milliseconds: 1000),
        dotSize: 6.0,
        dotBgColor: Colors.transparent,
        showIndicator: true,
        indicatorBgPadding: 7.0,
        images: covers.map((index) {
          return InkWell(
            child: Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage(index),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            onTap: () {
              _showImage(index);
            },
          );
        }).toList(),
      ),
    );
  }

  //INFORMACIÓN PRINCIPAL DE LA EMPRESA
  Widget _companyDetail(CompanyModel company) {
    return Column(
      children: <Widget>[
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Column(
              children: <Widget>[
                InkWell(
                  child: CircleAvatar(
                    radius: 45,
                    backgroundImage: NetworkImage(company.profileImage),
                  ),
                  onTap: () {
                    _showImage(company.profileImage);
                  },
                ),
                SizedBox(
                  height: 5.0,
                ),
                _companyRatingWidget(company),
              ],
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 20.0, bottom: 4.0),
                    child: Text(company.name,
                        style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold,
                            color: Color(0xFF4B4B4B))),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20.0, bottom: 12.0),
                    child: Wrap(
                      children: company.tags.map((tag) {
                        return Text(
                          tag + ' ',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            color: Color(0xFF4B4B4B),
                          ),
                        );
                      }).toList(),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 12.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        GrayCircleButton(
                            data: company.phone,
                            iconRuoute: 'assets/phoneicongray.png',
                            onTap: () {
                              _openUrl('tel://' + company.phone);
                            }),
                        GrayCircleButton(
                            data: company.mapLink,
                            iconRuoute: 'assets/locationicongray.png',
                            onTap: () {
                              _openUrl(company.mapLink);
                            }),
                        GrayCircleButton(
                          data: company.facebookLink,
                          iconRuoute: 'assets/facebookicongray.png',
                          onTap: () {
                            _openUrl(company.facebookLink);
                          },
                        ),
                        GrayCircleButton(
                          data: company.facebookLink,
                          iconRuoute: 'assets/save.png',
                          onTap: () {},
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            _destacadoIcon(company),
          ],
        ),
        Padding(
          padding: const EdgeInsets.only(top: 26.0, bottom: 26.0),
          child: Text(company.description),
        ),
      ],
    );
  }

  Widget _buildSchedule(CompanyModel company) {
    if (company.schedule != null) {
      var scheduleList = company.schedule.values.toList();
      var days = [
        'Lunes',
        'Martes',
        'Miércoles',
        'Jueves',
        'Viernes',
        'Sábado',
        'Domingo'
      ];
      return ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        itemCount: scheduleList.length,
        itemBuilder: (_, index) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 32.0),
            child: Padding(
              padding: const EdgeInsets.only(bottom: 5.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  GeneralText(text: days[index]),
                  GeneralText(text: scheduleList[index]),
                ],
              ),
            ),
          );
        },
      );
    } else {
      return Container();
    }
  }

  Widget _buildPromotionList(CompanyModel company) {
    List<DocumentSnapshot> documents;
    Future getPromotions() async {
      var firestore = Firestore.instance;
      QuerySnapshot q = await firestore
          .collection('promotion')
          // .where('companyId', isEqualTo: company.companyId)
          .getDocuments();
      documents = q.documents;
      return q.documents;
    }

    return Container(
      height: 195.0,
      width: MediaQuery.of(context).size.height,
      child: FutureBuilder(
          future: getPromotions(),
          builder: (_, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else {
              return Padding(
                padding: const EdgeInsets.all(0.0),
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: documents.length,
                  itemBuilder: (_, index) {
                    PromotionModel promotion = new PromotionModel();
                    promotion.image =
                        snapshot.data[index].data['image'].toString();
                    promotion.companyId =
                        snapshot.data[index].data['companyId'].toString();
                    return CustomPromoCard(
                        imagen: promotion.image,
                        onTap: () {
                          _showImage(promotion.image);
                        });
                  },
                ),
              );
            }
          }),
    );
  }

  Widget _textoParaUbicaciones(bool isCompany) {
    if (isCompany == false) {
      return Align(
        alignment: Alignment.centerLeft,
        child: TabText(text: 'Ubicacion'),
      );
    } else {
      return Align(
        alignment: Alignment.centerLeft,
        child: TabText(text: 'Sucursales'),
      );
    }
  }

  Widget _buildSurcursalList(CompanyModel company) {
    List<DocumentSnapshot> documents;
    var sucursalSize = 85.0;

    Future getSucursales() async {
      var firestore = Firestore.instance;
      QuerySnapshot q = await firestore
          .collection('company')
          .document(company.companyId)
          .collection('sucursal')
          .getDocuments();
      documents = q.documents;
      return q.documents;
    }

    return FutureBuilder(
        future: getSucursales(),
        builder: (_, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            return Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Container(
                height: sucursalSize * documents.length,
                child: ListView.builder(
                  padding: EdgeInsets.only(top: 16.0, bottom: 16.0),
                  //physics: NeverScrollableScrollPhysics(),
                  itemCount: documents.length,
                  itemBuilder: (_, index) {
                    SucursalModel sucursal = new SucursalModel();
                    //populate object
                    if (snapshot.data[index].data['sucursalId'].toString() ==
                        'none') {
                      sucursal.address = company.address;
                      sucursal.facebookLink = company.facebookLink;
                      sucursal.mapLink = company.mapLink;
                      sucursal.name = company.name;
                      sucursal.phone = company.phone;
                      sucursal.sucursalId = company.companyId;
                    } else {
                      sucursal.address =
                          snapshot.data[index].data['address'].toString();
                      sucursal.facebookLink =
                          snapshot.data[index].data['facebookLink'].toString();
                      sucursal.mapLink =
                          snapshot.data[index].data['mapLink'].toString();
                      sucursal.name =
                          snapshot.data[index].data['name'].toString();
                      sucursal.phone =
                          snapshot.data[index].data['phone'].toString();
                      sucursal.sucursalId =
                          snapshot.data[index].data['sucursalId'].toString();
                    }

                    return Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        _itemCardSucursal(sucursal),
                      ],
                    );
                  },
                ),
              ),
            );
          }
        });
  }

  Widget _itemCardSucursal(SucursalModel sucursal) {
    _openUrl(url) async {
      if (await canLaunch(url)) {
        await launch(url);
      } else {
        //throw 'Error no se puede abrir';
      }
    }

    return Padding(
      padding: const EdgeInsets.only(bottom: 20.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          InkWell(
            onTap: () {
              _openUrl(sucursal.mapLink);
            },
            child: Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Image(
                    height: 20.0,
                    image: AssetImage('assets/locationicongray.png'),
                  ),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        sucursal.name,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Color(0xFF4B4B4B)),
                      ),
                      Text(sucursal.address),
                    ],
                  ),
                ),
              ],
            ),
          ),
          InkWell(
            child: Padding(
              padding: const EdgeInsets.only(left: 30.0, top: 5.0),
              child: Text(
                sucursal.phone,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Color(0xFF3D82D3),
                  fontSize: 14.0,
                ),
              ),
            ),
            onTap: () {
              _openUrl('tel://' + sucursal.phone);
            },
          ),
        ],
      ),
    );
  }

  Widget _phoneCompany(CompanyModel company) {
    double tamano = MediaQuery.of(context).size.width - 60;
    if (company.phone == '' || company.phone == null) {
      return Center();
    } else {
      return Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                  left: 8.0, right: 0.0, top: 8, bottom: 8.0),
              child: Container(
                height: 20.0,
                width: 20.0,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/phoneicongray.png'),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ),
            ConstrainedBox(
              constraints:
                  BoxConstraints(maxHeight: 30, maxWidth: tamano, minWidth: 5),
              child: MaterialButton(
                onPressed: () {
                  _openUrl('tel://' + company.phone);
                },
                child: Text(
                  company.phone,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF3D82D3),
                    fontSize: 16.0,
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    }
  }

  Widget _emailCompany(CompanyModel company) {
    double tamano = MediaQuery.of(context).size.width - 60;
    if (company.email == '' || company.email == null) {
      return Center();
    } else {
      return Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                  left: 8.0, right: 0.0, top: 8, bottom: 8.0),
              child: Container(
                height: 20.0,
                width: 20.0,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/email.png'),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ),
            ConstrainedBox(
              constraints:
                  BoxConstraints(maxHeight: 30, maxWidth: tamano, minWidth: 5),
              child: MaterialButton(
                onPressed: () {
                  _sendMail(company.email, '', '');
                },
                child: Text(
                  company.email,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF3D82D3),
                    fontSize: 16.0,
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    }
  }

  Widget _webLinkCompany(CompanyModel company) {
    double tamano = MediaQuery.of(context).size.width - 60;
    if (company.webLink == '' || company.webLink == null) {
      return Center();
    } else {
      return Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                  left: 8.0, right: 0.0, top: 8, bottom: 8.0),
              child: Container(
                height: 20.0,
                width: 20.0,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/language.png'),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ),
            ConstrainedBox(
              constraints:
                  BoxConstraints(maxHeight: 30, maxWidth: tamano, minWidth: 5),
              child: MaterialButton(
                onPressed: () {
                  _openUrl(company.webLink);
                },
                child: Text(
                  company.webLink,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF3D82D3),
                    fontSize: 16.0,
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    }
  }

  Widget _companyRatingWidget(CompanyModel company) {
    return Row(
      children: <Widget>[
        ImageFromAssets(
            imgUrl: 'assets/full-star.png', height: 15.0, width: 15.0),
        SizedBox(width: 3.0),
        Text(
          company.rating.toString(),
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),
        ),
      ],
    );
  }

  Widget _destacadoIcon(CompanyModel company) {
    if (company.destacado) {
      return ImageFromAssets(
          imgUrl: 'assets/destacado-icon-yellow.png', height: 28, width: 28);
    } else {
      return Container();
    }
  }

  Widget _ratingReview(String companyId) {
    return FutureBuilder(
      future: _getCompanyRatingReview(),
      builder: (_, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          return RatingBar(
            initialRating: _companyRatingReview,
            direction: Axis.horizontal,
            allowHalfRating: false,
            itemCount: 5,
            ratingWidget: RatingWidget(
              full: ImageFromAssets(
                imgUrl: 'assets/full-star.png',
                height: 15.0,
                width: 15.0,
              ),
              half: ImageFromAssets(
                imgUrl: 'assets/full-star.png',
                height: 15.0,
                width: 15.0,
              ),
              empty: ImageFromAssets(
                imgUrl: 'assets/empty-star.png',
                height: 15.0,
                width: 15.0,
              ),
            ),
            itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
            onRatingUpdate: (rating) {
              ratingService.setCompanyReview(companyId, _userId, rating);
            },
          );
        } else {
          return Container();
        }
      },
    );
  }
}
