import 'package:flutter/material.dart';
import 'package:relapp_pruebas/plugins/widgets.dart';
import 'package:relapp_pruebas/services/auth_service.dart';
import 'package:relapp_pruebas/view/acerca_de.dart';
import 'package:relapp_pruebas/view/aviso_privacidad.dart';
import 'package:relapp_pruebas/view/contacto.dart';
import 'package:relapp_pruebas/view/perfil_usuario.dart';
import 'package:relapp_pruebas/view/registro_empresa.dart';

class Config extends StatelessWidget {
  const Config({Key key, this.auth, this.logoutCallback}) : super(key: key);

  final BaseAuth auth;
  final VoidCallback logoutCallback;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 100.0,
              width: MediaQuery.of(context).size.width,
              child: Container(
                decoration: BoxDecoration(boxShadow: [
                  BoxShadow(
                    color: Colors.black12,
                    blurRadius: 10.0,
                  ),
                ]),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),
                  child: Padding(
                    padding:
                        const EdgeInsets.only(left: 28.0, right: 28.0, top: 24),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'Configuración',
                          style: TextStyle(
                            color: Color(0xFF4B4B4B),
                            fontSize: 22,
                            fontFamily: 'GilroyExtraBold',
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            ConfigItemButton(
              title: 'Perfil',
              description: 'Configura tu perfil',
              iconRoute: 'assets/user-icon.png',
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => PerfilUsuario(
                            auth: auth, logoutCallback: logoutCallback)));
              },
            ),
            ConfigItemButton(
              title: 'Registra tu empresa',
              description: 'Aparece en nuestra app sin costo..',
              iconRoute: 'assets/register-icon.png',
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => RegistroEmpresa()));
              },
            ),
            ConfigItemButton(
              title: 'Contáctanos',
              description: 'Envíanos tus comentarios..',
              iconRoute: 'assets/email.png',
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Contacto()));
              },
            ),
            ConfigItemButton(
              title: 'Acerca de',
              description: 'Conócenos..',
              iconRoute: 'assets/acercade.png',
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => AcercaDe()));
              },
            ),
            ConfigItemButton(
              title: 'Aviso de privacidad',
              description: 'Términos y condiciones',
              iconRoute: 'assets/privacy.png',
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => AvisoPrivacidad()));
              },
            ),
          ],
        ),
      ),
    );
  }
}
