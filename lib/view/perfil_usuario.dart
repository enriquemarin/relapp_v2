import 'package:flutter/material.dart';
import 'package:relapp_pruebas/plugins/widgets.dart';
import 'package:relapp_pruebas/services/auth_service.dart';

class PerfilUsuario extends StatefulWidget {
  const PerfilUsuario({Key key, this.auth, this.logoutCallback})
      : super(key: key);

  final BaseAuth auth;
  final VoidCallback logoutCallback;

  @override
  _PerfilUsuarioState createState() => _PerfilUsuarioState();
}

class _PerfilUsuarioState extends State<PerfilUsuario> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  String _userName;
  String _email;
  String _photo;
  signOut() async {
    try {
      await widget.auth.signOut();
      widget.logoutCallback();
      Navigator.pop(context);
    } catch (e) {
      print(e);
    }
  }

  @override
  void initState() {
    super.initState();
    widget.auth.getCurrentUser().then((user) {
      setState(() {
        if (user != null) {
          _userName = user?.displayName;
          _email = user?.email;
          _photo = user?.photoUrl;
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Row(
            children: <Widget>[
              Text(
                "Perfil",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              )
            ],
          ),
        ),
        body: Center(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  height: 200.0,
                  width: 200.0,
                  child: CircleAvatar(
                    backgroundImage: NetworkImage(_photo),
                  ),
                ),
                SizedBox(height: 20.0),
                SubtitleText(text: _userName),
                GeneralText(text: _email),
                FlatButton(
                    child: new Text('Cerrar sesión',
                        style:
                            new TextStyle(fontSize: 17.0, color: Colors.black)),
                    onPressed: signOut)
              ]),
        ));
  }
}
