import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:quiver/strings.dart';
import 'package:url_launcher/url_launcher.dart';

class RegistroEmpresa extends StatefulWidget {
  RegistroEmpresa({Key key}) : super(key: key);

  _RegistroEmpresaState createState() => _RegistroEmpresaState();
}

class _RegistroEmpresaState extends State<RegistroEmpresa> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Row(
            children: <Widget>[
              Text(
                "Regístrate con nosotros",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              )
            ],
          ),
        ),
        body: Container(
          child: Padding(
            padding: const EdgeInsets.only(left: 14, right: 14, top: 32.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                RichText(
                    text: TextSpan(
                        style: Theme.of(context).textTheme.body1.copyWith(
                              fontSize: 18,
                            ),
                        children: [
                      TextSpan(
                          text:
                              'Si tienes una empresa o negocio y quieres ser parte de ',
                          style: TextStyle(color: Color(0xFF4B4B4B))),
                      TextSpan(
                          text: 'Rel',
                          style: TextStyle(
                              color: Color(0xFF703CFF),
                              fontWeight: FontWeight.bold)),
                      TextSpan(
                          text: ', llena nuestro formulario en ',
                          style: TextStyle(color: Color(0xFF4B4B4B))),
                      TextSpan(
                          text: 'www.relapp.mx ',
                           recognizer: TapGestureRecognizer()..onTap = () => _openUrl('http://relapp.mx/#contact'),
                          style: TextStyle(
                              color: Colors.blue, fontWeight: FontWeight.bold)),
                      TextSpan(
                          text: 'con los siguientes datos:',
                          style: TextStyle(color: Color(0xFF4B4B4B))),
                    ])),
                Padding(
                  padding: const EdgeInsets.only(top: 32.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('● Nombre de tu empresa',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18,
                              color: Color(0xFF4B4B4B))),
                      Padding(
                        padding: EdgeInsets.all(8.0),
                      ),
                      Text('● Datos de contacto',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18,
                              color: Color(0xFF4B4B4B))),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(8.0),
                ),
                Text(
                    'Una vez recibamos tus datos nosotros nos comunicaremos contigo para continuar con el proceso de registro y darte a conocer los beneficios que tenemos para ti.',
                    style: TextStyle(fontSize: 18, color: Color(0xFF4B4B4B)))
              ],
            ),
          ),
        ), //MyCustomForm(),
      ),
    );
  }
   _openUrl(url) async {
      if (await canLaunch(url)) {
        await launch(url);
      } else {
        //throw 'Error no se puede abrir';
      }
    }
}

/* class MyCustomForm extends StatefulWidget {
  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

class MyCustomFormState extends State<MyCustomForm> {
  final _formKey = GlobalKey<FormState>();
  //var _passKey = GlobalKey<FormFieldState>();
  String _nombreEmpresa = '';
  String _correo = '';
  String _mensaje = '';

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Form(
      key: _formKey,
      child: new ListView(
        children: <Widget>[
          new TextFormField(
            decoration: InputDecoration(
                //hintText: 'Nombre de la empresa',
                labelText: 'Nombre de la empresa'),
            validator: (value) {
              if (value.isEmpty) return 'Este campo no puede quedar vacío';
            },
            onSaved: (value) => _nombreEmpresa = value,
          ),
          new TextFormField(
            decoration: InputDecoration(
                //hintText: 'Nombre de la empresa',
                labelText: 'Correo electrónico'),
            validator: (value) {
              if (value.isEmpty) return 'Este campo no puede quedar vacío';
              if (!value.contains('@'))
                return 'Este no es un correo electrónico';
            },
            onSaved: (value) => _correo = value,
          ),
          new TextFormField(
            decoration: InputDecoration(
                //hintText: 'Nombre de la empresa',
                labelText: 'Mensaje'),
            validator: (value) {
              if (value.isEmpty) return 'Este campo no puede quedar vacío';
            },
            onSaved: (value) => _mensaje = value,
          ),
        ],
      ),
    );
  } */

  /*  List<Widget> getFormWidget() {
    List<Widget> formWidget = new List();
    formWidget.add(
      new TextFormField(
          decoration: InputDecoration(
              hintText: 'Nombre de la empresa',
              labelText: 'Ingrese nombre de su empresa'),
          validator: (value) {
            if (value.isEmpty) return 'Este campo no puede quedar vacío';
          }
          //onSaved: (value) => ,
          ),
    );

    formWidget.add(
      new TextFormField(
          obscureText: true,
          decoration: InputDecoration(
              hintText: 'Confirm Password',
              labelText: 'Enter Confirm Password'),
          validator: (confirmPassword) {
            if (confirmPassword.isEmpty) return 'Enter confirm password';
            var password = _passKey.currentState.value;
            if (!equalsIgnoreCase(confirmPassword, password))
              return 'Confirm Password invalid';
          },
          onSaved: (value) {
            setState(() {
              _password = value;
            });
          }),
    );

    return formWidget;
  } */
//}
