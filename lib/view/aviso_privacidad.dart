import 'package:flutter/material.dart';

class AvisoPrivacidad extends StatelessWidget {
  final String text = '1.1 El siguiente aviso de privacidad para empresas establece los términos sobre los cuales Relapp utiliza y protege la información que las empresas proporcionan para ser utilizada dentro de la aplicación móvil Relapp y el sitio web www.relapp.mx' +
      '\n\n' +
      '1.2 Relapp está comprometida al adecuado uso de la información con fines de publicidad y mercadotecnia exclusivamente, para los cuales Relapp asegura la protección y la divulgación solo con el consentimiento del titular de marca en caso de ser contenido privado y sin elconsentimiento en caso de ser contenido público (léase 4.1)' +
      '\n\n' +
      '2.1-. Nombre o razón social de la marca: nombre de la marca, referencias sociales, razón social, marcas fonéticas, juegos de palabras, nombres compuestos, etc.' +
      '\n\n' +
      '2.2-. Marca mixta, elemento diferenciador o logotipo de la empresa: Logotipo, isotipo, imagotipo, elementos diferenciadores de la marca, elementos asociativos de la marca, colores corporativos, formas exclusivas, variantes de marca, etc.' +
      '\n\n' +
      '2.3-. Material gráfico de la marca: Promociones, banners, imágenes compuestas, elementos publicitarios, ligas a otras marcas, etc.' +
      '\n\n' +
      '2.4-. Dirección de la empresa: Dirección fiscal de la empresa, puntos de venta, sucursales, etc.' +
      '\n\n' +
      '2.5-. Información de contacto de la empresa: Números telefónicos, dirección de páginas web, otras redes sociales, dirección a otras aplicaciones, etc.' +
      '\n\n' +
      '3.1 La información antes mencionada será usada como se mencionó en el apartado 1.2 con fines de publicidad y mercadotecnia por parte de Relapp con el fin de brindar difusión a la marca de una manera más sencilla, la información no contempla datos personales de los titulares de la marca a menos de que el tipo de empresa así lo exija o estos estén exhibidos en redes sociales como información pública (léase 4.1)' +
      '\n\n' +
      '4.1 En caso de los recursos gráficos, de información y contacto comprendidos en los apartados 2.1, 2.2, 2.3, 2.4 y 2.5 que se encuentren en redes sociales de la marca serán clasificados como contenido público y Relapp podrá hacer uso de esa información solo con fines de publicidad de la marca, así como la actualización continua de la información de marca.' +
      '\n\n' +
      '5.1 Relapp no se hace responsable por el uso que terceros a la aplicación y los cuales Relapp no reconozca como internos puedan darle a la información que se presenta en las plataformas Relapp y www.relapp.mx ';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Row(
          children: <Widget>[
            Text(
              "Aviso de privacidad",
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            )
          ],
        ),
      ),
      body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Container(
        child: Text(
            text,
            textAlign: TextAlign.justify,
            style: Theme.of(context).textTheme.body1.copyWith(fontSize: 18),
        ),
      ),
          )),
    );
  }
}
