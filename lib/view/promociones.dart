import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:relapp_pruebas/model/model_company.dart';
import 'package:relapp_pruebas/model/model_promotion.dart';
import 'package:relapp_pruebas/view/perfil_empresa.dart';

class Promociones extends StatelessWidget {
  const Promociones({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // List<DocumentSnapshot> documents;
    List<String> dias = [
      'Todos',
      'Lunes',
      'Martes',
      'Miércoles',
      'Jueves',
      'Viernes',
      'Sábado',
      'Domingo'
    ];
    TabController _tabController;

    /*   Future getPromotions() async {
      var firestore = Firestore.instance;
      QuerySnapshot q = await firestore.collection('promotion').getDocuments();
      documents = q.documents;
      return q.documents;
    } */

    return Scaffold(
      body: DefaultTabController(
        length: 8,
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            backgroundColor: Colors.white,
            automaticallyImplyLeading: false,
            elevation: 2.0,
            title: Align(
              alignment: Alignment.centerLeft,
              child: Text(
                'Promociones',
                style: TextStyle(
                  color: Color(0xFF4B4B4B),
                  fontSize: 22,
                  fontFamily: 'GilroyExtraBold',
                ),
              ),
            ),
            bottom: TabBar(
                labelColor: Color(0xFF4B4B4B),
                unselectedLabelColor: Color(0xFF4B4B4B),
                isScrollable: true,
                indicatorWeight: 5.0,
                indicatorColor: Color(0xFF4B4B4B),
                tabs: dias.map((index) {
                  return Tab(text: index);
                }).toList()),
          ),
          body: TabBarView(
            controller: _tabController,
            children: dias.map((index) {
              return Center(
                child: _builPromoList(index),
              );
            }).toList(),
          ),
        ),
      ),
    );
  }

  Widget _builPromoList(String dia) {
    List<DocumentSnapshot> documents;

    Future getPromotionsByDay() async {
      var firestore = Firestore.instance;
      QuerySnapshot q;
      if (dia == 'Todos') {
        q = await firestore.collection('promotion').getDocuments();
      } else {
        q = await firestore
            .collection('promotion')
            .where('day', isEqualTo: dia)
            .getDocuments();
      }

      documents = q.documents;
      return q.documents;
    }

    return Container(
      child: FutureBuilder(
          future: getPromotionsByDay(),
          builder: (_, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else {
              return Padding(
                padding: const EdgeInsets.all(0.0),
                child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: documents.length,
                  itemBuilder: (_, index) {
                    PromotionModel promotion = new PromotionModel();
                    promotion.image =
                        snapshot.data[index].data['image'].toString();
                    promotion.companyId =
                        snapshot.data[index].data['companyId'].toString();
                    return _promoCard(_, promotion);
                  },
                ),
              );
            }
          }),
    );
  }

  Widget _promoCard(BuildContext context, PromotionModel promotion) {
    Future getCompany() async {
      String companyId = promotion.companyId;
      var firestore = Firestore.instance;

      QuerySnapshot q = await firestore
          .collection('company')
          .where('companyId', isEqualTo: companyId)
          .getDocuments();
      return q.documents;
    }

    Future<void> _showDialog() async {
      return showDialog<void>(
        context: context,
        barrierDismissible: true, // user must tap button!
        builder: (BuildContext context) {
          return AlertDialog(
            contentPadding: const EdgeInsets.all(0),
            content: Container(
              child: Image(
                image: NetworkImage(promotion.image),
              ),
            ),
          );
        },
      );
    }

    return Padding(
      padding: const EdgeInsets.only(
          left: 32.0, right: 32.0, top: 24.0, bottom: 16.0),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(22.0),
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 26.0,
            ),
          ],
        ),
        child: InkWell(
          child: ClipRRect(
            borderRadius: BorderRadius.all(
              Radius.circular(22.0),
            ),
            child: AspectRatio(
              aspectRatio: 3 / 3,
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: NetworkImage(promotion.image),
                    fit: BoxFit.cover,
                  ),
                ),
                child: FutureBuilder(
                    future: getCompany(),
                    builder: (_, companySnapShot) {
                      if (companySnapShot.connectionState ==
                          ConnectionState.waiting) {
                        return Center();
                      } else {
                        CompanyModel company = new CompanyModel();
                        company.companyId = companySnapShot
                            .data[0].data['companyId']
                            .toString();
                        company.name =
                            companySnapShot.data[0].data['name'].toString();
                        company.profileImage = companySnapShot
                            .data[0].data['profileImage']
                            .toString();
                        company.description = companySnapShot
                            .data[0].data['description']
                            .toString();
                        company.email =
                            companySnapShot.data[0].data['email'].toString();
                        company.webLink =
                            companySnapShot.data[0].data['webLink'].toString();
                        company.mapLink =
                            companySnapShot.data[0].data['mapLink'].toString();
                        company.facebookLink = companySnapShot
                            .data[0].data['facebookLink']
                            .toString();
                        company.address =
                            companySnapShot.data[0].data['address'].toString();
                        company.phone =
                            companySnapShot.data[0].data['phone'].toString();
                        company.tags = companySnapShot.data[0].data['tag'];
                        company.covers = companySnapShot.data[0].data['cover'];
                        company.isCompany =
                            companySnapShot.data[0].data['isCompany'];
                        company.schedule =
                            companySnapShot.data[0].data['schedule'];
                        company.rating = double.parse(
                            companySnapShot.data[0].data['rating'].toString());
                        return _companyDetail(context, company);
                      }
                    }),
              ),
            ),
          ),
          onTap: () {
            _showDialog();
          },
        ),
      ),
    );
  }

  Widget _companyDetail(BuildContext context, CompanyModel company) {
    double tamano = MediaQuery.of(context).size.width - 160;
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          // Where the linear gradient begins and ends
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          // Add one stop for each color. Stops should increase from 0 to 1
          stops: [0.3, 0.9],
          colors: [
            // Colors are easy thanks to Flutter's Colors class.
            Color.fromARGB(0, 255, 255, 255),
            Colors.black45,
          ],
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          FlatButton(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                    padding: const EdgeInsets.only(left: 8.0, bottom: 8.0),
                    child: CircleAvatar(
                      backgroundImage: NetworkImage(company.profileImage),
                      radius: 20.0,
                    )),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0, bottom: 8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      ConstrainedBox(
                        constraints: BoxConstraints(
                            maxHeight: 30, maxWidth: tamano, minWidth: 5),
                        child: Text(
                          company.name,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.bold),
                        ),
                      ),
                      Wrap(
                        children: company.tags.map((tag) {
                          return Text(
                            tag + ' ',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          );
                        }).toList(),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => PerfilEmpresa(
                    company: company,
                  ),
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
