import 'package:flutter/material.dart';

class NotificationContent extends StatelessWidget {
  const NotificationContent({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final arg = ModalRoute.of(context).settings.arguments;

    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Contenido de la notificación'),
        ),
        body: Center(
          child: Text(arg),
        ),
      ),
    );
  }
} 
