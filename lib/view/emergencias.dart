import 'package:flutter/material.dart';
import 'package:relapp_pruebas/model/model_categoria.dart';
import 'package:relapp_pruebas/model/model_emergencia.dart';
import 'package:url_launcher/url_launcher.dart';

class Emergencias extends StatefulWidget {
   CategoriaModel categoriaModel;

  Emergencias({Key key, this.categoriaModel}) : super(key: key);

  _EmergenciasState createState() => _EmergenciasState();
}

class _EmergenciasState extends State<Emergencias> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(widget.categoriaModel.nombreCategoria,  style: TextStyle(
              color: widget.categoriaModel.colordefondo,
              fontSize: 22,
              fontFamily: 'GilroyExtraBold',
            ),),),
      body: _listaEmergencias(widget.categoriaModel.nombreCategoria),
    );
  }

  Widget _listaEmergencias(String categoria){

    if (categoria == 'Salud') {
      return SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Text('La Paz',
                style: TextStyle(
                  fontSize: 22,
                  fontFamily: 'GilroyExtraBold', 
                )),
              ),
              Column(
                children: serviciosSaludLaPaz.map((item){
                  return _itemEmergencias(item);
                }).toList(),
              ),
            ],
          ),
        ),
      );
    }
    if (categoria == 'Seguridad') {
      return SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Text('La Paz',
                style: TextStyle(
                  fontSize: 22,
                  fontFamily: 'GilroyExtraBold', 
                )),
              ),
              Column(
                children: serviciosSeguridadLaPaz.map((item){
                  return _itemEmergencias(item);
                }).toList(),
              ),
            ],
          ),
        ),
      );
    }
    if (categoria == 'Públicos') {
      return SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Text('La Paz',
                style: TextStyle(
                  fontSize: 22,
                  fontFamily: 'GilroyExtraBold', 
                )),
              ),
              Column(
                children: serviciosPublicosLaPaz.map((item){
                  return _itemEmergencias(item);
                }).toList(),
              ),
            ],
          ),
        ),
      );
    }
  }
}

Widget _itemEmergencias(EmergenciaModel item){
    return Padding(
      padding: const EdgeInsets.only(bottom: 12.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(item.nombre, style: TextStyle(fontSize: 14.0),),
          InkWell(child: Text(item.telefono, style: TextStyle(fontSize: 16.0,color: Colors.blue, fontWeight: FontWeight.bold),),
          onTap: (){
            launch('tel://'+item.telefono);
          },)
        ],
      ),
    );
}