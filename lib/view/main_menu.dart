import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:relapp_pruebas/plugins/widgets.dart';
import 'package:relapp_pruebas/services/auth_service.dart';
import 'package:relapp_pruebas/view/categorias.dart' as categorias;
import 'package:relapp_pruebas/view/config.dart' as config;
import 'package:relapp_pruebas/view/inicio.dart' as inicio;
import 'package:relapp_pruebas/view/promociones.dart' as promociones;
import 'package:relapp_pruebas/view/mapa.dart' as mapa;

class MainMenu extends StatefulWidget {
  MainMenu({Key key,this.title, this.auth, this.userId, this.logoutCallback}) : super(key: key);

  final String title;
   final BaseAuth auth;
  final VoidCallback logoutCallback;
  final String userId;
  
  @override
  _MainMenu createState() => _MainMenu();
}

class _MainMenu extends State<MainMenu> with SingleTickerProviderStateMixin {
   final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: DefaultTabController(
        length: 5,
        child: Scaffold(
          body: TabBarView(
            physics: NeverScrollableScrollPhysics(),
            children: <Widget>[
              inicio.Inicio(),
              categorias.Categorias(),
              mapa.Mapa(),
              promociones.Promociones(),
              config.Config(auth: widget.auth, logoutCallback: widget.logoutCallback),
            ],
          ),
          bottomNavigationBar: CustomBottomNavigationBar(),
          backgroundColor: Colors.white,
        ),
      ),
    );
  }
}
