import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class AcercaDe extends StatelessWidget {
  AcercaDe({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Row(
          children: <Widget>[
            Text(
              "Acerca de",
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            )
          ],
        ),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 36.0),
              child: Image(
                height: 68.0,
                image: AssetImage('assets/logorel.png'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 16.0),
              child: Container(
                child: Column(
                  children: <Widget>[
                    Text(
                      'Buscador de empresas y promociones',
                      style: TextStyle(
                        color: Color(0xFF4B4B4B),
                        fontSize: 17,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      'Síguenos en:',
                      style: TextStyle(
                        color: Color(0xFF4B4B4B),
                        fontSize: 17,
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 16.0),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        RawMaterialButton(
                          onPressed: () {
                            _openUrl('fb://page/617416045432415');
                          },
                          child: new Image(
                            height: 26.0,
                            image: AssetImage('assets/face.png'),
                          ),
                          shape: new CircleBorder(),
                          elevation: 2.0,
                          fillColor: Color(0xFF3C5A99),
                          padding: const EdgeInsets.all(15.0),
                        ),
                        RawMaterialButton(
                          onPressed: () {
                            _openUrl('instagram://user?username=relcodelabs');
                          },
                          child: new Image(
                            height: 26.0,
                            image: AssetImage('assets/insta.png'),
                          ),
                          shape: new CircleBorder(),
                          elevation: 2.0,
                          fillColor: Color(0xFFF73772),
                          padding: const EdgeInsets.all(15.0),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 16.0),
                    ),
                    Text(
                      'o visita nuestra página web',
                      style: TextStyle(
                        color: Color(0xFF4B4B4B),
                        fontSize: 16,
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                    FlatButton(
                      onPressed: () {
                        _openUrl('https://www.relapp.mx');
                      },
                      child: Text(
                        "www.relapp.mx",
                        style: TextStyle(color: Colors.blue, fontSize: 19.0),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 36.0),
              child: Text("Powered by"),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
              child: Image(
                height: 68.0,
                image: AssetImage('assets/asynclogo.png'),
              ),
            ),
            FlatButton(
              onPressed: () {
                _openUrl('https://www.relapp.mx/privacy-policy');
              },
              child: Text(
                "Políticas de privacidad",
                style: TextStyle(color: Colors.blue, fontSize: 12.0),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _openUrl(url) async {
    if (await canLaunch(url)) {
      await launch(url, forceWebView: false);
    } else {
      throw 'Error no se puede abrir';
    }
  }
}
