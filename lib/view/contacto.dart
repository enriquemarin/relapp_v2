import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class Contacto extends StatefulWidget {
  Contacto({Key key}) : super(key: key);

  _ContactoState createState() => _ContactoState();
}

class _ContactoState extends State<Contacto> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Row(
          children: <Widget>[
            Text(
              "Contáctanos",
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            )
          ],
        ),
      ),
      body: Container(
          child: Padding(
            padding: const EdgeInsets.only(left: 14, right: 14, top: 32.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                RichText(
                    text: TextSpan(
                        style: Theme.of(context).textTheme.body1.copyWith(
                              fontSize: 18,
                            ),
                        children: [
                      TextSpan(
                          text:
                              'Si tienes alguna duda o te gustaria reportar algun problema con respecto a ',
                          style: TextStyle(color: Color(0xFF4B4B4B))),
                      TextSpan(
                          text: 'Rel',
                          style: TextStyle(
                              color: Color(0xFF703CFF),
                              fontWeight: FontWeight.bold)),
                      TextSpan(
                          text: ', llena nuestro formulario en ',
                          style: TextStyle(color: Color(0xFF4B4B4B))),
                      TextSpan(
                          text: 'www.relapp.mx ',
                          recognizer: TapGestureRecognizer()..onTap = () => _openUrl('http://relapp.mx/#contact'),
                          style: TextStyle(
                              color: Colors.blue, fontWeight: FontWeight.bold)),
                      TextSpan(
                          text: 'y nosotros le daremos seguimiento a tu petición.',
                          style: TextStyle(color: Color(0xFF4B4B4B))),
                    ])),
              ],
            ),
          ),
        ), //MyCustomForm(),
    );
  }
   _openUrl(url) async {
      if (await canLaunch(url)) {
        await launch(url);
      } else {
        //throw 'Error no se puede abrir';
      }
    }
}