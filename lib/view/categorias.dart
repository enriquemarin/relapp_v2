import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:relapp_pruebas/plugins/widgets.dart';
import 'package:relapp_pruebas/model/model_categoria.dart';
import 'package:relapp_pruebas/services/category_service.dart';

class Categorias extends StatefulWidget {
  @override
  _Categorias createState() => new _Categorias();
}

class _Categorias extends State<Categorias> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: DefaultTabController(
        length: 8,
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            backgroundColor: Colors.white,
            automaticallyImplyLeading: false,
            elevation: 2.0,
            title: Align(
              alignment: Alignment.centerLeft,
              child: TitleText(text: 'Categorias'),
            ),
          ),
          body: _builCategoryList(),
        ),
      ),
    );
  }

  Widget _builCategoryList() {
    return ListView(
      children: <Widget>[
        SizedBox(
          height: 160,
          child: Column(
            children: <Widget>[
              Padding(
                padding:
                    const EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    SubtitleText(text: 'Destacados'),
                    ViewMoreButton(onTap: () {}),
                  ],
                ),
              ),
              new Expanded(
                child: _buildDestacados(),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SubtitleText(text: "Comercio"),
              Padding(padding: EdgeInsets.only(bottom: 16.0),),
              GridView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                 // crossAxisSpacing: 32.0,
                  mainAxisSpacing: 22.0,
                ),
                itemCount: categoriasComercio.length,
                itemBuilder: (_, index) {
                  return CustomCircleCategory(categoriaModel: categoriasComercio[index]);
                },
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SubtitleText(text: "Para mí"),
              Padding(padding: EdgeInsets.only(bottom: 16.0),),
              GridView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  crossAxisSpacing: 5.0,
                  mainAxisSpacing: 5.0,
                ),
                itemCount: categoriasParaMi.length,
                itemBuilder: (context, index) {
                  return CustomCircleCategory(categoriaModel: categoriasParaMi[index]);
                },
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SubtitleText(text: "Negocios"),
              Padding(padding: EdgeInsets.only(bottom: 16.0),),
              GridView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  crossAxisSpacing: 5.0,
                  mainAxisSpacing: 5.0,
                ),
                itemCount: categoriasNegocios.length,
                itemBuilder: (context, index) {
                  return CustomCircleCategory(categoriaModel: categoriasNegocios[index]);
                },
              ),
            ],
          ),
        ),
         Padding(
          padding: const EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SubtitleText(text: "Descubre"),
              Padding(padding: EdgeInsets.only(bottom: 16.0),),
              GridView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  crossAxisSpacing: 5.0,
                  mainAxisSpacing: 5.0,
                ),
                itemCount: categoriasDescubre.length,
                itemBuilder: (context, index) {
                  return CustomCircleCategory(categoriaModel: categoriasDescubre[index]);
                },
              ),
            ],
          ),
        ),
         Padding(
          padding: const EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SubtitleText(text: "Emergencias"),
              Padding(padding: EdgeInsets.only(bottom: 16.0),),
              GridView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  crossAxisSpacing: 5.0,
                  mainAxisSpacing: 5.0,
                ),
                itemCount: categoriasEmergencia.length,
                itemBuilder: (context, index) {
                  return CustomCircleCategory(categoriaModel: categoriasEmergencia[index]);
                },
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildDestacados() {
    List<DocumentSnapshot> documents;

    Future getDestacados() async {
      var firestore = Firestore.instance;
      QuerySnapshot q;
      q = await firestore.collection('destacados').getDocuments();
      documents = q.documents;
      return q.documents;
    }

    return Container(
      child: FutureBuilder(
          future: getDestacados(),
          builder: (_, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else {
              return ListView.builder(
                scrollDirection: Axis.horizontal,
                padding: const EdgeInsets.only(
                    left: 16.0, right: 16.0, top: 8, bottom: 8),
                itemCount: documents.length,
                itemBuilder: (_, index) {
                  var imagen = snapshot.data[index].data['image'].toString();
                  return CustomDestacadosEmpresas(
                    imagen: imagen,
                  );
                },
              );
            }
          }),
    );
  }
}

//PARA MOSTRAR MENSAJES AL FONDO DEL SCREEN
/* final snackBar = SnackBar(
  content: Text('Mira!! Un mensaje, pinche vato solo'),
  action: SnackBarAction(
    label: 'Undo',
    onPressed: () {
    },
  ),
);
Scaffold.of(context).showSnackBar(snackBar);  */
