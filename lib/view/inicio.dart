import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:relapp_pruebas/plugins/carrouselBanner.dart';
import 'package:relapp_pruebas/model/model_categoria.dart';
import 'package:relapp_pruebas/plugins/widgets.dart';

class Inicio extends StatefulWidget {
  Inicio({Key key}) : super(key: key);

  _InicioState createState() => _InicioState();
}

class _InicioState extends State<Inicio> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: <Widget>[
          new ListView(
            scrollDirection: Axis.vertical,
            children: <Widget>[
              SizedBox(
                height: 100,
              ),
              CarouselBanner(),
              SizedBox(
                height: 178,
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 28.0, right: 16.0, top: 24.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          SubtitleText(text: 'Guardados'),
                          ViewMoreButton(onTap: () {}),
                        ],
                      ),
                    ),
                    new Expanded(
                      child: new ListView(
                        scrollDirection: Axis.horizontal,
                        padding: const EdgeInsets.only(
                            left: 16.0, right: 16.0, top: 8, bottom: 8),
                        children: <Widget>[
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: _buildGuardados(),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 260,
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 28.0, right: 16.0, top: 16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          SubtitleText(text: '¿Qué hay para hoy?'),
                          ViewMoreButton(onTap: () {}),
                        ],
                      ),
                    ),
                    new Expanded(
                      child: _buildcards(),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 260,
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 28.0, right: 16.0, top: 16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          SubtitleText(text: 'Eventos en la ciudad'),
                          ViewMoreButton(onTap: () {}),
                        ],
                      ),
                    ),
                    Flexible(
                      child: _buildEvents(),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 160,
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 28.0, right: 16.0, top: 16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          SubtitleText(text: 'Destacados'),
                          ViewMoreButton(onTap: () {}),
                        ],
                      ),
                    ),
                    new Expanded(
                      child: _buildDestacados(),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 590,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 28.0, right: 16.0, top: 16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          SubtitleText(text: 'Explora Baja California Sur'),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 28.0, right: 16.0, top: 8.0),
                      child: Row(
                        children: <Widget>[
                          GeneralText(
                              text: 'Encuentra las mejores actividades y destinos turísticos para hacer  de tu visita una experiencia inolvidable.'),
                        ],
                      ),
                    ),
                    new Expanded(
                      child: _buildActivities(),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left:28.0, right: 16.0),
                      child: PurpleButton(text: 'Conoce más', onTap: (){},),
                    )
                  ],
                ),
              ),
            ],
          ),
          Positioned(
            top: 48,
            child: CustomAppBar(),
          ),
        ],
      ),
    );
  }

  List<Widget> _buildGuardados() {
    List<Widget> cards = [];
    for (int i = 0; i < categoriasComercio.length; i++) {
      cards
          .add(CustomCircleHomeCategory(categoriaModel: categoriasComercio[i]));
    }
    return cards;
  }

  Widget _buildcards() {
    List<DocumentSnapshot> documents;
    var hoy = new DateTime.now();

    String getDay() {
      switch (hoy.weekday) {
        case 1:
          {
            return 'Lunes';
          }
          break;
        case 2:
          {
            return 'Martes';
          }
          break;
        case 3:
          {
            return 'Miercoles';
          }
          break;
        case 4:
          {
            return 'Jueves';
          }
          break;
        case 5:
          {
            return 'Viernes';
          }
          break;
        case 6:
          {
            return 'Sabado';
          }
          break;
        case 7:
          {
            return 'Domingo';
          }
          break;
        default:
          {
            return 'Todos';
          }
      }
    }

    Future getPromotions() async {
      var firestore = Firestore.instance;
      QuerySnapshot q;
      q = await firestore
          .collection('promotion')
          //.where('day', isEqualTo: getDay())
          .getDocuments();
      documents = q.documents;
      return q.documents;
    }

    return Container(
      child: FutureBuilder(
          future: getPromotions(),
          builder: (_, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else {
              return ListView.builder(
                scrollDirection: Axis.horizontal,
                padding: const EdgeInsets.only(
                    left: 16.0, right: 16.0, top: 8, bottom: 8),
                itemCount: documents.length,
                itemBuilder: (_, index) {
                  var imagen = snapshot.data[index].data['image'].toString();
                  return CustomPromoCard(imagen: imagen, onTap: (){});
                },
              );
            }
          }),
    );
  }

  Widget _buildEvents() {
    List<DocumentSnapshot> documents;

    Future getPromotions() async {
      var firestore = Firestore.instance;
      QuerySnapshot q;
      q = await firestore.collection('events').getDocuments();
      documents = q.documents;
      return q.documents;
    }

    return Container(
      child: FutureBuilder(
          future: getPromotions(),
          builder: (_, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else {
              return ListView.builder(
                scrollDirection: Axis.horizontal,
                padding: const EdgeInsets.only(
                    left: 16.0, right: 16.0, top: 8, bottom: 8),
                itemCount: documents.length,
                itemBuilder: (_, index) {
                  var imagen = snapshot.data[index].data['image'].toString();
                  var fecha = snapshot.data[index].data['dateTime'];
                  var titulo = snapshot.data[index].data['title'].toString();
                  var descripcion =
                      snapshot.data[index].data['description'].toString();
                  var subtitulo =
                      snapshot.data[index].data['subtitle'].toString();
                  return CustomEventCard(
                    imagen: imagen,
                    fecha: fecha,
                    titulo: titulo,
                    subtitulo: subtitulo,
                    descripcion: descripcion,
                  );
                },
              );
            }
          }),
    );
  }

  Widget _buildActivities() {
    List<DocumentSnapshot> documents;

    Future getPromotions() async {
      var firestore = Firestore.instance;
      QuerySnapshot q;
      q = await firestore.collection('events').getDocuments();
      documents = q.documents;
      return q.documents;
    }

    return Container(
      child: FutureBuilder(
          future: getPromotions(),
          builder: (_, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else {
              return ListView.builder(
                scrollDirection: Axis.horizontal,
                padding: const EdgeInsets.only(
                    left: 16.0, right: 16.0, top: 8, bottom: 16),
                itemCount: documents.length,
                itemBuilder: (_, index) {
                  return CustomActivities();
                },
              );
            }
          }),
    );
  }

  Widget _buildDestacados() {
    List<DocumentSnapshot> documents;

    Future getDestacados() async {
      var firestore = Firestore.instance;
      QuerySnapshot q;
      q = await firestore.collection('destacados').getDocuments();
      documents = q.documents;
      return q.documents;
    }

    return Container(
      child: FutureBuilder(
          future: getDestacados(),
          builder: (_, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(
                child: CircularProgressIndicator(),
              );
            } else {
              return ListView.builder(
                scrollDirection: Axis.horizontal,
                padding: const EdgeInsets.only(
                    left: 16.0, right: 16.0, top: 8, bottom: 8),
                itemCount: documents.length,
                itemBuilder: (_, index) {
                  var imagen = snapshot.data[index].data['image'].toString();
                  return CustomDestacadosEmpresas(
                    imagen: imagen,
                  );
                },
              );
            }
          }),
    );
  }
}
