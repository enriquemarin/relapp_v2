class UserModel {
  String uid;
  String userName;
  String email;
  String photoUrl;

  UserModel({this.uid, this.userName, this.email, this.photoUrl});
}
