import 'package:flutter/material.dart';

class CategoriaModel {
  String nombreCategoria;
  String descripcion;
  Icon icono;
  Image imagen;
  Color colordefondo;
  List<dynamic> subcategorias;

  CategoriaModel(
      {Key key,
      this.nombreCategoria,
      this.descripcion,
      this.icono,
      this.imagen,
      this.colordefondo,
      this.subcategorias});
}

List<CategoriaModel> categoriasEmergencia = [
  new CategoriaModel(
    nombreCategoria: "Salud",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/salud.png'),
      height: 86.0,
    ),
    colordefondo: Color(0xFFDE2525),
  ),
  new CategoriaModel(
    nombreCategoria: "Seguridad",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/seguridad.png'),
      height: 86.0,
    ),
    colordefondo: Color(0xFF2D6CE2),
  ),
  new CategoriaModel(
    nombreCategoria: "Públicos",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/emergencias.png'),
      height: 86.0,
    ),
    colordefondo: Color(0xFF4B4B4B),
  ),
];

List<CategoriaModel> categoriasComercio = [
  new CategoriaModel(
    nombreCategoria: "Restaurantes",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/restaurantes.png'),
      height: 86.0,
    ),
    colordefondo: Color(0xFFE22D50),
  ),
  new CategoriaModel(
    nombreCategoria: "Shopping",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/shoping.png'),
      height: 86.0,
    ),
    colordefondo: Color(0xFF703CFF),
  ),
  new CategoriaModel(
    nombreCategoria: "Regalos",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/regalos.png'),
      height: 86.0,
    ),
    colordefondo: Color(0xFFF74497),
  ),
   new CategoriaModel(
    nombreCategoria: "Alimentos",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/alimentos.png'),
      height: 86.0,
    ),
    colordefondo: Color(0xFFAD8065),
  ),
];

List<CategoriaModel> categoriasParaMi = [
  new CategoriaModel(
    nombreCategoria: "Fitness",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/fitness.png'),
      height: 86.0,
    ),
    colordefondo: Color(0xFF39C35E),
  ),
  new CategoriaModel(
    nombreCategoria: "Bienestar",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/bienestar.png'),
      height: 86.0,
    ),
    colordefondo: Color(0xFF3B8EF0),
  ),
  new CategoriaModel(
    nombreCategoria: "Relajación",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/relajacion.png'),
      height: 86.0,
    ),
    colordefondo: Color(0xFFE08CE3),
  ),
  new CategoriaModel(
    nombreCategoria: "Estilo",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/cuidadoestilo.png'),
      height: 86.0,
    ),
    colordefondo: Color(0xFFABC736),
  ),
  new CategoriaModel(
    nombreCategoria: "Educación",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/educacion.png'),
      height: 86.0,
    ),
    colordefondo: Color(0xFFD1B071),
  ),
  new CategoriaModel(
    nombreCategoria: "Sociales",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/sociales.png'),
      height: 86.0,
    ),
    colordefondo: Color(0xFF6A3B63),
  ),
  new CategoriaModel(
    nombreCategoria: "Deportes",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/deportes.png'),
      height: 86.0,
    ),
    colordefondo: Color(0xFF22A4AF),
  ),
  new CategoriaModel(
    nombreCategoria: "Mascotas",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/mascotas.png'),
      height: 86.0,
    ),
    colordefondo: Color(0xFFF7564F),
  ),
  new CategoriaModel(
    nombreCategoria: "Hogar",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/hogar.png'),
      height: 86.0,
    ),
    colordefondo: Color(0xFFB3D171),
  ),
];

List<CategoriaModel> categoriasNegocios = [
  new CategoriaModel(
    nombreCategoria: "Servicios",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/servicios.png'),
      height: 86.0,
    ),
    colordefondo: Color(0xFF2FBEA7),
  ),
  new CategoriaModel(
    nombreCategoria: "Construcción",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/construccion.png'),
      height: 86.0,
    ),
    colordefondo: Color(0xFF2E2EAF),
  ),
  new CategoriaModel(
    nombreCategoria: "Campo",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/campo.png'),
      height: 86.0,
    ),
    colordefondo: Color(0xFFDEB630),
  ),
   new CategoriaModel(
    nombreCategoria: "Automotriz",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/automotriz.png'),
      height: 86.0,
    ),
    colordefondo: Color(0xFFDD841C),
  ),
   new CategoriaModel(
    nombreCategoria: "Digital",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/digital.png'),
      height: 86.0,
    ),
    colordefondo: Color(0xFF5F6C8E),
  ),
];

List<CategoriaModel> categoriasDescubre = [
 new CategoriaModel(
    nombreCategoria: "Cultura",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/cultura.png'),
      height: 86.0,
    ),
    colordefondo: Color(0xFFE65799),
  ),
   new CategoriaModel(
    nombreCategoria: "Turismo",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/turismo.png'),
      height: 86.0,
    ),
    colordefondo: Color(0xFF29DDCF),
  ),
   new CategoriaModel(
    nombreCategoria: "Playas",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/playas.png'),
      height: 86.0,
    ),
    colordefondo: Color(0xFFED9C60),
  ),
   new CategoriaModel(
    nombreCategoria: "Hospedaje",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/hospedaje.png'),
      height: 86.0,
    ),
    colordefondo: Color(0xFFED6080),
  ),
   new CategoriaModel(
    nombreCategoria: "Hates",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/hates.png'),
      height: 86.0,
    ),
    colordefondo: Color(0xFFFF4D3E),
  ),
   new CategoriaModel(
    nombreCategoria: "Otros",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/otros.png'),
      height: 86.0,
    ),
    colordefondo: Color(0xFF4B4B4B),
  ),
];
/////////////////////////////////////////

var _miniHeight = 50.0;
List<CategoriaModel> categoriasForSearchMapLista = [
  /* new CategoriaModel(
    nombreCategoria: "Destacados",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/destacados.png'),
      height: _miniHeight,
    ),
    colordefondo: Color(0xFFE3B006),
    //subcategorias: ['Todos', 'Ejemplo1', 'Ejemplo2'],
  ), */
  new CategoriaModel(
    nombreCategoria: "Restaurantes",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/restaurantes.png'),
      height: _miniHeight,
    ),
    colordefondo: Color(0xFFE22D50),
    //subcategorias: ['Todos', 'Ejemplo1', 'Ejemplo2', 'Ejemplo3', 'Ejemplo1', 'Ejemplo2', 'Ejemplo3','Ejemplo1', 'Ejemplo2', 'Ejemplo3'],
  ),
  new CategoriaModel(
    nombreCategoria: "Shopping",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/shoping.png'),
      height: _miniHeight,
    ),
    colordefondo: Color(0xFF703CFF),
    //subcategorias: ['Todos', 'Ejemplo1', 'Ejemplo2'],
  ),
  new CategoriaModel(
    nombreCategoria: "Servicios",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/servicios.png'),
      height: _miniHeight,
    ),
    colordefondo: Color(0xFF2FBEA7),
    //subcategorias: ['Todos', 'Ejemplo1', 'Ejemplo2'],
  ),
  new CategoriaModel(
    nombreCategoria: "Sociales",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/sociales.png'),
      height: _miniHeight,
    ),
    colordefondo: Color(0xFF6A3B63),
    //subcategorias: ['Todos', 'Ejemplo1', 'Ejemplo2'],
  ),
  new CategoriaModel(
    nombreCategoria: "Fitness",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/fitness.png'),
      height: _miniHeight,
    ),
    colordefondo: Color(0xFF39C35E),
    //subcategorias: ['Todos', 'Ejemplo1', 'Ejemplo2'],
  ),
  new CategoriaModel(
    nombreCategoria: "Cultura",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/cultura.png'),
      height: _miniHeight,
    ),
    colordefondo: Color(0xFFE65799),
    //subcategorias: ['Todos', 'Ejemplo1', 'Ejemplo2'],
  ),
  new CategoriaModel(
    nombreCategoria: "Bienestar",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/bienestar.png'),
      height: _miniHeight,
    ),
    colordefondo: Color(0xFF3B8EF0),
    //subcategorias: ['Todos', 'Ejemplo1', 'Ejemplo2'],
  ),
  new CategoriaModel(
    nombreCategoria: "Alimentos",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/alimentos.png'),
      height: _miniHeight,
    ),
    colordefondo: Color(0xFFAD8065),
    //subcategorias: ['Todos', 'Ejemplo1', 'Ejemplo2'],
  ),
  new CategoriaModel(
    nombreCategoria: "Automotriz",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/automotriz.png'),
      height: _miniHeight,
    ),
    colordefondo: Color(0xFFDD841C),
    //subcategorias: ['Todos', 'Ejemplo1', 'Ejemplo2'],
  ),
  new CategoriaModel(
    nombreCategoria: "Estilo",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/cuidadoestilo.png'),
      height: _miniHeight,
    ),
    colordefondo: Color(0xFFABC736),
    //subcategorias: ['Todos', 'Ejemplo1', 'Ejemplo2'],
  ),
  new CategoriaModel(
    nombreCategoria: "Relajación",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/relajacion.png'),
      height: _miniHeight,
    ),
    colordefondo: Color(0xFFE08CE3),
    //subcategorias: ['Todos', 'Ejemplo1', 'Ejemplo2'],
  ),
  new CategoriaModel(
    nombreCategoria: "Mascotas",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/mascotas.png'),
      height: _miniHeight,
    ),
    colordefondo: Color(0xFFF7564F),
    //subcategorias: ['Todos', 'Ejemplo1', 'Ejemplo2'],
  ),
  new CategoriaModel(
    nombreCategoria: "Construcción",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/construccion.png'),
      height: _miniHeight,
    ),
    colordefondo: Color(0xFF2E2EAF),
    //subcategorias: ['Todos', 'Ejemplo1', 'Ejemplo2'],
  ),
  new CategoriaModel(
    nombreCategoria: "Campo",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/campo.png'),
      height: _miniHeight,
    ),
    colordefondo: Color(0xFFDEB630),
    //subcategorias: ['Todos', 'Ejemplo1', 'Ejemplo2'],
  ),
  new CategoriaModel(
    nombreCategoria: "Regalos",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/regalos.png'),
      height: _miniHeight,
    ),
    colordefondo: Color(0xFFF74497),
    //subcategorias: ['Todos', 'Ejemplo1', 'Ejemplo2'],
  ),
  new CategoriaModel(
    nombreCategoria: "Deportes",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/deportes.png'),
      height: _miniHeight,
    ),
    colordefondo: Color(0xFF22A4AF),
    //subcategorias: ['Todos', 'Ejemplo1', 'Ejemplo2'],
  ),
  new CategoriaModel(
    nombreCategoria: "Educación",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/educacion.png'),
      height: _miniHeight,
    ),
    colordefondo: Color(0xFFD1B071),
    //subcategorias: ['Todos', 'Ejemplo1', 'Ejemplo2'],
  ),
  new CategoriaModel(
    nombreCategoria: "Hogar",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/hogar.png'),
      height: _miniHeight,
    ),
    colordefondo: Color(0xFFB3D171),
    //subcategorias: ['Todos', 'Ejemplo1', 'Ejemplo2'],
  ),
  new CategoriaModel(
    nombreCategoria: "Digital",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/digital.png'),
      height: _miniHeight,
    ),
    colordefondo: Color(0xFF5F6C8E),
    //subcategorias: ['Todos', 'Ejemplo1', 'Ejemplo2'],
  ),
  new CategoriaModel(
    nombreCategoria: "Turismo",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/turismo.png'),
      height: _miniHeight,
    ),
    colordefondo: Color(0xFF29DDCF),
    //subcategorias: ['Todos', 'Ejemplo1', 'Ejemplo2'],
  ),
  new CategoriaModel(
    nombreCategoria: "Hospedaje",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/hospedaje.png'),
      height: _miniHeight,
    ),
    colordefondo: Color(0xFFED6080),
    //subcategorias: ['Todos', 'Ejemplo1', 'Ejemplo2'],
  ),
  new CategoriaModel(
    nombreCategoria: "Playas",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/playas.png'),
      height: _miniHeight,
    ),
    colordefondo: Color(0xFFED9C60),
    //subcategorias: ['Todos', 'Ejemplo1', 'Ejemplo2'],
  ),
  new CategoriaModel(
    nombreCategoria: "Hates",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/hates.png'),
      height: _miniHeight,
    ),
    colordefondo: Color(0xFFFF4D3E),
    //subcategorias: ['Todos', 'Ejemplo1', 'Ejemplo2'],
  ),
  new CategoriaModel(
    nombreCategoria: "Otros",
    descripcion: "...",
    icono: Icon(Icons.build),
    imagen: Image(
      image: AssetImage('assets/categorias/otros.png'),
      height: _miniHeight,
    ),
    colordefondo: Color(0xFF4B4B4B),
    //subcategorias: ['Todos', 'Ejemplo1', 'Ejemplo2'],
  ),
];
