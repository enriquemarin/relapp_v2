import 'package:flutter/material.dart';

class EmergenciaModel {
  final String nombre;
  final String telefono;

  EmergenciaModel(
      {Key key,
      this.nombre,
      this.telefono,});
}

List<EmergenciaModel> serviciosSaludLaPaz = [
   new EmergenciaModel(
      nombre: 'Cruz Roja Mexicana General',
      telefono: '065',
    ),
    new EmergenciaModel(
      nombre: 'Cruz Roja Mexicana',
      telefono: '1221111',
    ),
     new EmergenciaModel(
      nombre: 'Hospital Gral. Juan M. Salvatierra',
      telefono: '6121750500',
    ),
     new EmergenciaModel(
      nombre: 'IMSS',
      telefono: '6121220373',
    ),
    new EmergenciaModel(
      nombre: 'ISSSTE',
      telefono: '6131360255',
    ),
];

List<EmergenciaModel> serviciosSeguridadLaPaz = [
    new EmergenciaModel(
      nombre: 'Emergencias',
      telefono: '911',
    ),
    new EmergenciaModel(
      nombre: 'Angeles Verdes',
      telefono: '078',
    ),
   new EmergenciaModel(
      nombre: 'Heroico Cuerpo de Bomberos',
      telefono: '1654343',
    ),
    new EmergenciaModel(
      nombre: 'Policía Ministerial del Estado',
      telefono: '1257541',
    ),
     new EmergenciaModel(
      nombre: 'Policía Federal',
      telefono: '1146597',
    ),
     new EmergenciaModel(
      nombre: 'Policía Estatal',
      telefono: '1750400',
    ),
    new EmergenciaModel(
      nombre: 'Protección Civil Municipal',
      telefono: '1213634',
    ),
    new EmergenciaModel(
      nombre: 'Protección Civil Estatal',
      telefono: '1229008',
    ),
];

List<EmergenciaModel> serviciosPublicosLaPaz = [
  new EmergenciaModel(
    nombre: 'H. XV Ayuntamiento de La Paz',
    telefono: '6121237900',
  ),
  new EmergenciaModel(
    nombre: 'DIF Estatal',
    telefono: '6121242922',
  ),
   new EmergenciaModel(
    nombre: 'Registro Civil',
    telefono: '6121242850',
  ),
   new EmergenciaModel(
    nombre: 'OMSAPAS',
    telefono: '6121238600',
  ),
   new EmergenciaModel(
    nombre: 'ITAI',
    telefono: '6121210135',
  )
];