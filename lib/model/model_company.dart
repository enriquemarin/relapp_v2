
class CompanyModel {
  CompanyModel(
      {this.companyId,
      this.category,
      this.address,
      this.name,
      this.description,
      this.email,
      this.phone,
      this.facebookLink,
      this.webLink,
      this.mapLink,
      this.profileImage,
      this.subcategory,
      this.covers,
      this.tags,
      this.schedule,
      this.geopoint,
      this.isCompany,
      this.destacado,
      this.rating});

  String companyId;
  String category;
  String address;
  String name;
  String description;
  String email;
  String phone;
  String facebookLink;
  String webLink;
  String mapLink;
  String profileImage;
  String subcategory;
  List<dynamic> covers;
  List<dynamic> tags;
  var schedule;
  var geopoint;
  bool isCompany;
  bool destacado;
  double rating;
}

