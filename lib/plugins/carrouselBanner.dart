import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class CarouselBanner extends StatefulWidget {
  CarouselBanner({Key key}) : super(key: key);

  @override
  _CarouselBannerState createState() => _CarouselBannerState();
}

class _CarouselBannerState extends State<CarouselBanner> {
  List<DocumentSnapshot> documents;

  Future getMainBanner() async {
    var firestore = Firestore.instance;
    QuerySnapshot q = await firestore.collection('mainBanner').getDocuments();
    documents = q.documents;
    return q.documents;
  }

   Future _showImage(String image) async {
    return showDialog(
      context: context,
      barrierDismissible: true, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: const EdgeInsets.all(0),
          content: Container(
            child: Image(
              image: NetworkImage(image),
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 140,
      child: FutureBuilder(
        future: getMainBanner(),
        builder: (_, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            return _carousel(context, documents, snapshot);
          }
        },
      ),
    );
  }

  //Widget para el carrusel principal
  Widget _carousel(BuildContext context, List<DocumentSnapshot> documents,
      AsyncSnapshot snapshot) {
    var images = new List<dynamic>();

    if (documents.length != 0) {
      for (var i = 0; i < documents.length; i++) {
        images.add(snapshot.data[i].data['image'].toString());
      }

      return CarouselSlider(
        autoPlay: true,
        autoPlayAnimationDuration: Duration(seconds: 1),
        items: images.map((index) {
          if (index != null) {
            return InkWell(
                          child: ClipRRect(
                borderRadius: BorderRadius.all(
                  Radius.circular(25.0),
                ),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(25.0),
                    ),
                  ),
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.symmetric(horizontal: 5.0),
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(
                      Radius.circular(25.0),
                    ),
                    child: FadeInImage.assetNetwork(
                      placeholder: 'assets/loading2.gif',
                      image: index,
                      fit: BoxFit.fitHeight,
                    ),
                  ),
                ),
              ),
              onTap: (){_showImage(index);},
            );
          } else {
            return null;
          }
        }).toList(),
        height: 120,
        aspectRatio: 1 / 3,
      );
    } else {
      return Center(child: Text('Error al cargar...'));
    }
  }
}
