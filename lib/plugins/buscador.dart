import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:relapp_pruebas/model/model_company.dart';
import 'package:relapp_pruebas/view/perfil_empresa.dart';

class Buscador extends SearchDelegate<String> {
  List<CompanyModel> tempSearchStore;
  //var companies = new List<Company>();

  @override
  ThemeData appBarTheme(BuildContext context) {
    assert(context != null);
    final ThemeData theme = Theme.of(context);
    assert(theme != null);
    return theme;
  }

  Future powerSearch(query) async {
    var companies = new List<CompanyModel>();
    tempSearchStore = new List<CompanyModel>();
    QuerySnapshot getCompanies =
        await Firestore.instance.collection('company').getDocuments();

    for (var item in getCompanies.documents) {
      companies.add(new CompanyModel(
          companyId: item.data['companyId'].toString(),
          name: item.data['name'].toString(),
          description: item.data['description'].toString(),
          email: item.data['email'].toString(),
          category: item.data['category'].toString(),
          address: item.data['address'].toString(),
          subcategory: item.data['subcategory'].toString(),
          phone: item.data['phone'].toString(),
          facebookLink: item.data['facebookLink'].toString(),
          mapLink: item.data['mapLink'].toString(),
          webLink: item.data['webLink'].toString(),
          profileImage: item.data['profileImage'].toString(),
          covers: item.data['cover'],
          tags: item.data['tag'],
          schedule: item.data['schedule'],
          isCompany: item.data['isCompany'],
          destacado: item.data['destacado']));
    }

    tempSearchStore = companies
        .where((c) =>
            c.name.toLowerCase().contains(query.toString().toLowerCase()) ||
            c.category.toLowerCase() == query.toString().toLowerCase() ||
            c.subcategory.toLowerCase() == query.toString().toLowerCase() ||
            c.tags
                .toString()
                .toLowerCase()
                .contains(query.toString().toLowerCase()))
        .toList();
  }

  @override
  String get hint => 'My hint text';

  @override
  List<Widget> buildActions(BuildContext context) {
    // TODO: implement buildActions
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // TODO: implement buildLeading
    return IconButton(
      icon: Icon(Icons.arrow_back),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    // TODO: implement buildResults
    return FutureBuilder(
        future: powerSearch(query), // initiateSearch(query),
        builder: (_, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            return ListView.builder(
                scrollDirection: Axis.vertical,
                itemCount: tempSearchStore.length,
                itemBuilder: (_, element) {
                  return _companyDetail(context, tempSearchStore[element]);
                });
          }
        });
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // TODO: implement buildSuggestions
    return FutureBuilder(
        future: powerSearch(query),
        builder: (_, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            return ListView.builder(
                scrollDirection: Axis.vertical,
                itemCount: tempSearchStore.length,
                itemBuilder: (_, element) {
                  return _companyDetail(context, tempSearchStore[element]);
                });
          }
        });
  }
}

Widget _companyDetail(BuildContext context, CompanyModel company) {
  return Padding(
    padding: const EdgeInsets.only(left: 16.0, right: 16.0),
    child: Container(
      color: Colors.white,
      height: 50.0,
      child: FlatButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => PerfilEmpresa(company: company),
            ),
          );
        },
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            RichText(
              text: TextSpan(
                text: company.name,
                style:
                    TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      ),
    ),
  );
}
