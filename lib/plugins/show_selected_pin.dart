import 'package:google_maps_flutter/google_maps_flutter.dart';

class ShowSelectedPin {
  BitmapDescriptor showPin(String category) {
    switch (category) {
      case 'Restaurantes':
        return BitmapDescriptor.fromAsset(
            "assets/maPines/restaurantes-pin.png");
        break;
      case 'Alimentos':
        return BitmapDescriptor.fromAsset("assets/maPines/alimentos-pin.png");
        break;
      case 'Automotriz':
        return BitmapDescriptor.fromAsset("assets/maPines/automotriz-pin.png");
        break;
      case 'Bienestar':
        return BitmapDescriptor.fromAsset("assets/maPines/bienestar-pin.png");
      case 'Campo':
        return BitmapDescriptor.fromAsset("assets/maPines/campo-pin.png");
        break;
      case 'Construcción':
        return BitmapDescriptor.fromAsset(
            "assets/maPines/construccion-pin.png");
        break;
      case 'Cultura':
        return BitmapDescriptor.fromAsset("assets/maPines/cultura-pin.png");
        break;
      case 'Deportes':
        return BitmapDescriptor.fromAsset("assets/maPines/desportes-pin.png");
        break;
      case 'Digital':
        return BitmapDescriptor.fromAsset("assets/maPines/digital-pin.png");
        break;
      case 'Educación':
        return BitmapDescriptor.fromAsset("assets/maPines/educacion-pin.png");
        break;
      case 'Estilo':
        return BitmapDescriptor.fromAsset("assets/maPines/estilo-pin.png");
        break;
      case 'Fitness':
        return BitmapDescriptor.fromAsset("assets/maPines/fitness-pin.png");
        break;
      case 'Hates':
        return BitmapDescriptor.fromAsset("assets/maPines/hates-pin.png");
        break;
      case 'Hogar':
        return BitmapDescriptor.fromAsset("assets/maPines/hogar-pin.png");
        break;
      case 'Hospedaje':
        return BitmapDescriptor.fromAsset("assets/maPines/hospedaje-pin.png");
        break;
      case 'Mascotas':
        return BitmapDescriptor.fromAsset("assets/maPines/mascotas-pin.png");
        break;
      case 'Playas':
        return BitmapDescriptor.fromAsset("assets/maPines/playas-pin.png");
        break;
      case 'Regalos':
        return BitmapDescriptor.fromAsset("assets/maPines/regalos-pin.png");
        break;
      case 'Relajación':
        return BitmapDescriptor.fromAsset("assets/maPines/relajacion-pin.png");
        break;
      case 'Servicios':
        return BitmapDescriptor.fromAsset("assets/maPines/servicios-pin.png");
        break;
      case 'Shopping':
        return BitmapDescriptor.fromAsset("assets/maPines/shopping-pin.png");
        break;
      case 'Sociales':
        return BitmapDescriptor.fromAsset("assets/maPines/sociales-pin.png");
        break;
      case 'Turismo':
        return BitmapDescriptor.fromAsset("assets/maPines/turismo-pin.png");
        break;
      default:
        return BitmapDescriptor.fromAsset("assets/hate.png");
    }
  }
}
