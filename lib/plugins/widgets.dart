import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:relapp_pruebas/model/model_categoria.dart';
import 'package:relapp_pruebas/model/model_company.dart';
import 'package:relapp_pruebas/view/categoria.dart';
import 'package:relapp_pruebas/view/emergencias.dart';
import 'package:relapp_pruebas/view/perfil_empresa.dart';
import 'package:url_launcher/url_launcher.dart';
import 'buscador.dart';
import 'customicons.dart';

class TitleText extends StatelessWidget {
  final String text;
  const TitleText({Key key, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        color: Color(0xFF4B4B4B),
        fontSize: 22,
        fontFamily: 'GilroyExtraBold',
      ),
    );
  }
}

class SubtitleText extends StatelessWidget {
  final String text;
  const SubtitleText({Key key, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: new TextStyle(
        fontSize: 20.0,
        fontWeight: FontWeight.bold,
        color: Color(0xFF4B4B4B),
      ),
    );
  }
}

class TabText extends StatelessWidget {
  final String text;
  const TabText({Key key, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: new TextStyle(
        fontSize: 16.0,
        fontWeight: FontWeight.bold,
        color: Color(0xFF4B4B4B),
      ),
    );
  }
}

class GeneralText extends StatelessWidget {
  final String text;
  const GeneralText({Key key, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: Text(
        this.text,
        style: new TextStyle(
          fontSize: 16.0,
          fontWeight: FontWeight.normal,
          color: Color(0xFF4B4B4B),
        ),
      ),
    );
  }
}

class ViewMoreButton extends StatelessWidget {
  final Function onTap;
  const ViewMoreButton({Key key, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.circular(50.0),
      focusColor: Colors.purpleAccent,
      splashColor: Colors.deepPurple,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          'Ver Todos',
          style: new TextStyle(
            fontSize: 16.0,
            fontWeight: FontWeight.bold,
            color: Color(0xFF703CFF),
          ),
        ),
      ),
      onTap: this.onTap,
    );
  }
}

class PurpleButton extends StatelessWidget {
  final String text;
  final Function onTap;
  const PurpleButton({Key key, this.text, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FlatButton(
        //borderRadius: BorderRadius.circular(50.0),
        focusColor: Colors.purpleAccent,
        splashColor: Colors.deepPurple,
        textColor: Colors.white,
        color: Color(0xFF8282D8),
        shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(18.0),
            side: BorderSide(color: Color(0xFF8282D8))),
        onPressed: onTap,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            text,
            style: new TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.bold,
              color: Colors.white,
            ), /* Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            color: Color(0xFF8282D8),
                            child: Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Text(
                                'Conoce más',
                                style: new TextStyle(
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),*/
          ),
        ),
      ),
    );
  }
}

class GrayCircleButton extends StatelessWidget {
  final String data;
  final String iconRuoute;
  final Function onTap;
  const GrayCircleButton({Key key, this.data, this.iconRuoute, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (data != null && data != '') {
      return LimitedBox(
        maxWidth: 50.0,
        child: RawMaterialButton(
          onPressed: onTap,
          child: new Image(
            height: 18.0,
            image: AssetImage(iconRuoute),
          ),
          shape: new CircleBorder(),
          elevation: 0.0,
          fillColor: Color(0xFFEAEAEA),
          padding: const EdgeInsets.all(12.0),
        ),
      );
    } else {
      return Container();
    }
  }
}

class CustomAppBar extends StatelessWidget {
  const CustomAppBar({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width - 48,
      margin: const EdgeInsets.only(left: 24, right: 24),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 8.0,
          ),
        ],
        borderRadius: BorderRadius.circular(12.0),
      ),
      child: InkWell(
        child: Padding(
          padding: const EdgeInsets.only(
              left: 24.0, right: 24.0, top: 16.0, bottom: 16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Rel',
                style: TextStyle(
                  color: Color(0xFF703CFF),
                  fontSize: 20,
                  fontFamily: 'GilroyExtraBold',
                ),
              ),
              Text(
                'Busca lo que necesitas',
                style: TextStyle(
                  color: Colors.black38,
                  fontSize: 14,
                  fontFamily: 'Gilroy',
                ),
              ),
              Icon(
                Icons.search,
                color: Colors.black54,
              ),
            ],
          ),
        ),
        onTap: () {
          showSearch(context: context, delegate: Buscador());
          /*   final snackBar = SnackBar(
            content: Text('Yay! A SnackBar!'),
            action: SnackBarAction(
              label: 'Undo',
              onPressed: () {
                // Some code to undo the change.
              },
            ),
          );

          // Find the Scaffold in the widget tree and use
          // it to show a SnackBar.
          Scaffold.of(context).showSnackBar(snackBar); */
        },
      ),
    );
  }
}

class CustomPromoCard extends StatelessWidget {
  final String imagen;
  final Function onTap;
  CustomPromoCard({this.imagen, this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        width: 160.0,
        height: 160.0,
        margin: const EdgeInsets.only(
            left: 10.0, right: 10.0, top: 10, bottom: 22.0),
        decoration: new BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 20.0,
              offset: Offset(
                0.0,
                10.0,
              ),
            ),
          ],
          borderRadius: BorderRadius.all(
            Radius.circular(20.0),
          ),
          color: Colors.white,
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.all(
            Radius.circular(20.0),
          ),
          child: FadeInImage.assetNetwork(
            placeholder: 'assets/loading.png',
            image: imagen,
            fit: BoxFit.fitHeight,
          ),
        ),
      ),
      onTap: this.onTap,
    );
  }
}

class CustomEventCard extends StatelessWidget {
  final String imagen;
  final Timestamp fecha;
  final String titulo;
  final String subtitulo;
  final String descripcion;
  CustomEventCard(
      {this.imagen, this.fecha, this.titulo, this.subtitulo, this.descripcion});

  String mesDeFecha(int mes) {
    switch (mes) {
      case 1:
        {
          return 'Ene';
        }
        break;
      case 2:
        {
          return 'Feb';
        }
        break;
      case 3:
        {
          return 'Mar';
        }
        break;
      case 4:
        {
          return 'Abr';
        }
        break;
      case 5:
        {
          return 'May';
        }
        break;
      case 6:
        {
          return 'Jun';
        }
        break;
      case 7:
        {
          return 'Jul';
        }
        break;
      case 8:
        {
          return 'Ago';
        }
        break;
      case 9:
        {
          return 'Sep';
        }
        break;
      case 10:
        {
          return 'Oct';
        }
        break;
      case 11:
        {
          return 'Nov';
        }
        break;
      case 12:
        {
          return 'Dic';
        }
        break;
      default:
        {
          return 'Mes';
        }
    }
  }

  @override
  Widget build(BuildContext context) {
    DateTime date = fecha.toDate();
    String dateSlug =
        "${date.day.toString().padLeft(2, '0')} ${mesDeFecha(date.month)} ${date.year.toString()}";
    return Container(
      width: 300.0,
      height: 160.0,
      margin:
          const EdgeInsets.only(left: 10.0, right: 10.0, top: 10, bottom: 22.0),
      decoration: new BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 20.0,
            offset: Offset(
              0.0,
              10.0,
            ),
          ),
        ],
        borderRadius: BorderRadius.all(
          Radius.circular(20.0),
        ),
        color: Colors.white,
      ),
      child: Row(
        children: <Widget>[
          AspectRatio(
            aspectRatio: 1 / 1,
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: ClipRRect(
                borderRadius: BorderRadius.all(
                  Radius.circular(16.0),
                ),
                child: Container(
                  child: FadeInImage.assetNetwork(
                    placeholder: 'assets/loading.png',
                    image: imagen,
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
                left: 0, right: 12.0, top: 12.0, bottom: 12.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 18,
                      child: ConstrainedBox(
                        constraints: BoxConstraints(
                            maxHeight: 18, maxWidth: 126, minWidth: 5),
                        child: Text(
                          titulo,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                          style: TextStyle(
                            color: Colors.black,
                            fontFamily: 'GilroyExtraBold',
                            fontSize: 15.0,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 22,
                      child: ConstrainedBox(
                        constraints: BoxConstraints(
                            maxHeight: 18, maxWidth: 126, minWidth: 5),
                        child: Text(
                          subtitulo,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                          style: TextStyle(
                            color: Colors.blueGrey,
                            fontWeight: FontWeight.bold,
                            fontSize: 13.0,
                          ),
                        ),
                      ),
                    ),
                    ConstrainedBox(
                      constraints: BoxConstraints(
                          maxHeight: 60, maxWidth: 126, minWidth: 5),
                      child: Text(
                        descripcion,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 4,
                        style: TextStyle(
                          color: Colors.black45,
                          fontSize: 13.0,
                        ),
                      ),
                    ),
                  ],
                ),
                Text(
                  dateSlug,
                  style: TextStyle(
                    color: Colors.green,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class CustomBottomNavigationBar extends StatelessWidget {
  const CustomBottomNavigationBar({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double myHeight = 60.0; //Your height HERE
    return SizedBox(
      height: myHeight,
      width: MediaQuery.of(context).size.width,
      child: Container(
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 10.0,
            ),
          ],
        ),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          child: Padding(
            padding: const EdgeInsets.only(bottom: 0.0),
            child: TabBar(
              tabs: [
                Tab(
                  icon: Icon(
                    MyFlutterApp.home,
                    size: 21.0,
                    color: Color(0xFF626262),
                  ),
                ),
                Tab(
                  icon: Icon(
                    MyFlutterApp.categorias,
                    size: 21.0,
                    color: Color(0xFF626262),
                  ),
                ),
                Tab(
                  icon: Icon(
                    MyFlutterApp.pin,
                    size: 21.0,
                    color: Color(0xFF626262),
                  ),
                ), 
                Tab(
                  icon: Icon(
                    MyFlutterApp.promos,
                    size: 21.0,
                    color: Color(0xFF626262),
                  ),
                ),
                Tab(
                  icon: Icon(
                    MyFlutterApp.config,
                    size: 21.0,
                    color: Color(0xFF626262),
                  ),
                ),
              ],
              indicatorColor: Color(0xFF703CFF),
              indicatorWeight: 5.0,
            ),
          ),
        ),
      ),
    );
  }
}

class CustomCircleCategory extends StatelessWidget {
  final CategoriaModel categoriaModel;
  const CustomCircleCategory({Key key, this.categoriaModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.circular(50.0),
      focusColor: Colors.purpleAccent,
      splashColor: Color(0xFF703CFF),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: <Widget>[
            Container(
              decoration: new BoxDecoration(
                  shape: BoxShape.circle, color: categoriaModel.colordefondo),
              child: categoriaModel.imagen,
            ),
            Text(
              categoriaModel.nombreCategoria,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                fontSize: 13.0,
                color: Color(0xFF4B4B4B),
              ),
            ),
          ],
        ),
      ),
      onTap: () {
        if (categoriaModel.nombreCategoria == 'Salud' ||
            categoriaModel.nombreCategoria == 'Seguridad' ||
            categoriaModel.nombreCategoria == 'Públicos') {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (BuildContext context) =>
                  Emergencias(categoriaModel: categoriaModel),
            ),
          );
        } else {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (BuildContext context) => Categoria(
                category: categoriaModel,
              ),
            ),
          );
        }
      },
    );
  }
}

class CustomCircleHomeCategory extends StatelessWidget {
  final CategoriaModel categoriaModel;
  const CustomCircleHomeCategory({Key key, this.categoriaModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.circular(50.0),
      focusColor: Colors.purpleAccent,
      splashColor: Color(0xFF703CFF),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: <Widget>[
            Container(
              decoration: new BoxDecoration(
                  shape: BoxShape.circle, color: categoriaModel.colordefondo),
              child: categoriaModel.imagen,
            )
          ],
        ),
      ),
      onTap: () {
        if (categoriaModel.nombreCategoria == 'Salud' ||
            categoriaModel.nombreCategoria == 'Seguridad' ||
            categoriaModel.nombreCategoria == 'Públicos') {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (BuildContext context) =>
                  Emergencias(categoriaModel: categoriaModel),
            ),
          );
        } else {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (BuildContext context) => Categoria(
                category: categoriaModel,
              ),
            ),
          );
        }
      },
    );
  }
}

class CustomCircleMiniCategory extends StatelessWidget {
  final CategoriaModel categoriaModel;
  final Function onTap;
  const CustomCircleMiniCategory({Key key, this.categoriaModel, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.circular(50.0),
      focusColor: Colors.purpleAccent,
      splashColor: Color(0xFF703CFF),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: <Widget>[
            Container(
              decoration: new BoxDecoration(
                  shape: BoxShape.circle, color: categoriaModel.colordefondo),
              child: categoriaModel.imagen,
            )
          ],
        ),
      ),
      onTap: onTap,
    );
  }
}

class CustomDestacadosEmpresas extends StatelessWidget {
  final String imagen;
  const CustomDestacadosEmpresas({Key key, this.imagen}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.circular(60.0),
      focusColor: Colors.purpleAccent,
      splashColor: Color(0xFF703CFF),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          height: 32,
          decoration: new BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.brown,
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(50.0),
            child: Image.network(imagen),
          ),
        ),
      ),
      onTap: () {},
    );
  }
}

class CustomActivities extends StatelessWidget {
  const CustomActivities({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300.0,
      height: 360.0,
      margin:
          const EdgeInsets.only(left: 10.0, right: 10.0, top: 10, bottom: 22.0),
      decoration: new BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 28.0,
            offset: Offset(
              0.0,
              10.0,
            ),
          ),
        ],
        borderRadius: BorderRadius.all(
          Radius.circular(28.0),
        ),
        color: Colors.white,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(28), topRight: Radius.circular(28)),
            child: AspectRatio(
              aspectRatio: 1 / 1,
              child: Container(
                child: Image.network(
                  "https://img-aws.ehowcdn.com/560x560p/s3-us-west-1.amazonaws.com/contentlab.studiod/getty/70a153dfc82f48f3a9342013255ef521",
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 16.0, top: 12.0),
            child: Text(
              "Camping Isla Espíritu Santo",
              style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 16, top: 6),
            child: Row(
              children: <Widget>[
                Image(
                  image: AssetImage('assets/star.png'),
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Text(
                    "4.6",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Color(0xFF703CFF),
                      fontSize: 18.0,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class CustomCompanyCard extends StatelessWidget {
  final CompanyModel company;

  const CustomCompanyCard({Key key, this.company}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double tamano = MediaQuery.of(context).size.width - 160;
    _openUrl(url) async {
      if (await canLaunch(url)) {
        await launch(url, forceWebView: false);
      } else {
        throw 'Error no se puede abrir';
      }
    }

    return Padding(
      padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Center(
                  child: MaterialButton(
                    child: InkWell(
                      child: Row(
                        children: <Widget>[
                          ClipRRect(
                            borderRadius: BorderRadius.all(
                              Radius.circular(22.5),
                            ),
                            child: Container(
                              height: 45,
                              width: 45,
                              margin: EdgeInsets.only(bottom: 5.0),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(22.5),
                                ),
                                image: DecorationImage(
                                  image: NetworkImage(company.profileImage),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                ConstrainedBox(
                                  constraints: BoxConstraints(
                                      maxHeight: 30,
                                      maxWidth: tamano,
                                      minWidth: 5),
                                  child: Text(
                                    company.name,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                    style: TextStyle(
                                        color: Color(0xFF4B4B4B),
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                Wrap(
                                  children: company.tags.map((tag) {
                                    return Text(
                                      tag + ' ',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                        color: Color(0xFF4B4B4B),
                                      ),
                                    );
                                  }).toList(),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => PerfilEmpresa(company: company),
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
            //Imagen
            InkWell(
              child: Container(
                height: 250.0,
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.symmetric(vertical: 5.0),
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: NetworkImage(company.covers[0]),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => PerfilEmpresa(company: company),
                  ),
                );
              },
            ),
            //Botones
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                GrayCircleButton(
                  data: company.phone,
                  iconRuoute: 'assets/phoneicongray.png',
                  onTap: () {
                    _openUrl('tel://' + company.phone);
                  },
                ),
                GrayCircleButton(
                    data: company.mapLink,
                    iconRuoute: 'assets/locationicongray.png',
                    onTap: () {
                      _openUrl(company.mapLink);
                    }),
                GrayCircleButton(
                    data: company.facebookLink,
                    iconRuoute: 'assets/facebookicongray.png',
                    onTap: () {
                      _openUrl(company.facebookLink);
                    }),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class ConfigItemButton extends StatelessWidget {
  final String title;
  final String description;
  final String iconRoute;
  final Function onTap;
  const ConfigItemButton(
      {Key key, this.title, this.description, this.iconRoute, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 24.0),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          MaterialButton(
            padding: const EdgeInsets.only(
                top: 8, bottom: 8.0, left: 16.0, right: 16.0),
            onPressed: onTap,
            child: Row(
              children: <Widget>[
                Image(
                  image: AssetImage(iconRoute),
                  height: 24,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 16.0),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text(
                          title,
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 18.0),
                          textAlign: TextAlign.start,
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Text(
                          description,
                          style: TextStyle(
                              fontWeight: FontWeight.normal, fontSize: 14.0),
                        ),
                      ],
                    )
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class ImageFromAssets extends StatelessWidget {
  final String imgUrl;
  final double height;
  final double width;
  const ImageFromAssets({Key key, this.imgUrl, this.height, this.width}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: height,
        width: width,
        margin: EdgeInsets.symmetric(vertical: 5.0),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(imgUrl),
            fit: BoxFit.cover,
          ),
        ),
      );
  }
}
//BOTONES DE LOS EVENTOS - LUGAR Y GUARDADO
/* Row(
  children: <Widget>[
    LimitedBox(
      maxWidth: 50.0,
      child: RawMaterialButton(
        onPressed: () {},
        child: new Image(
          height: 18.0,
          image: AssetImage('assets/locationicongray.png'),
        ),
        shape: new CircleBorder(),
        elevation: 0.0,
        fillColor: Color(0xFFEAEAEA),
        padding: const EdgeInsets.all(12.0),
      ),
    ),
    LimitedBox(
      maxWidth: 50.0,
      child: RawMaterialButton(
        onPressed: () {},
        child: new Image(
          height: 18.0,
          image: AssetImage('assets/save.png'),
        ),
        shape: new CircleBorder(),
        elevation: 0.0,
        fillColor: Color(0xFFEAEAEA),
        padding: const EdgeInsets.all(12.0),
      ),
    ),
  ],
), */
