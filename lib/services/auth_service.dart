import 'dart:async';
import 'dart:convert';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:http/http.dart' as http;
import 'package:relapp_pruebas/model/mode_user.dart';

abstract class BaseAuth {
  Future<String> signInAnon();

  Future<String> signInWhitFacebook();

  Future<String> signIn(String email, String password);

  Future<String> signUp(String email, String password);

  Future<FirebaseUser> getCurrentUser();

  Future<void> sendEmailVerification();

  Future<void> signOut();

  Future<bool> isEmailVerified();
}

class Auth implements BaseAuth {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

 // get http => null;

  Future<String> signInAnon() async {
    AuthResult result = await _firebaseAuth.signInAnonymously();
    FirebaseUser user = result.user;
    return user.uid;
  }

  Future<String> signInWhitFacebook() async {
    FacebookLogin _loginFacebook = FacebookLogin();
    bool isLogin = false;
    var result = await _loginFacebook.logIn(['email']);
    switch (result.status) {
      case FacebookLoginStatus.error:
        print('Login error');
        isLogin = false;
        break;
      case FacebookLoginStatus.cancelledByUser:
        print('Cancelado por el usuario');
        isLogin = false;
        break;
      case FacebookLoginStatus.loggedIn:
        isLogin = true;
        break;
      default:
    }

    if (isLogin) {
      var token = result.accessToken.token;
      var graphResponse = await http.get('https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token=${token}');
      var profile = json.decode(graphResponse.body);
      var facebookAuthCred = FacebookAuthProvider.getCredential(accessToken: token);
     
      AuthResult authresult = await _firebaseAuth.signInWithCredential(facebookAuthCred);
      FirebaseUser user = authresult.user;
      return user.uid;
    } else {
      return '';
    }
  }

  Future<String> signIn(String email, String password) async {
    AuthResult result = await _firebaseAuth.signInWithEmailAndPassword(
        email: email, password: password);
    FirebaseUser user = result.user;
    return user.uid;
  }

  Future<String> signUp(String email, String password) async {
    AuthResult result = await _firebaseAuth.createUserWithEmailAndPassword(
        email: email, password: password);
    FirebaseUser user = result.user;
    return user.uid;
  }

  Future<FirebaseUser> getCurrentUser() async {
    FirebaseUser user = await _firebaseAuth.currentUser();
    return user;
  }

  Future<void> signOut() async {
    return _firebaseAuth.signOut();
  }

  Future<void> sendEmailVerification() async {
    FirebaseUser user = await _firebaseAuth.currentUser();
    user.sendEmailVerification();
  }

  Future<bool> isEmailVerified() async {
    FirebaseUser user = await _firebaseAuth.currentUser();
    return user.isEmailVerified;
  }
}
