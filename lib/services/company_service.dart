

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:relapp_pruebas/model/model_company.dart';

class CompanyService{
   final Firestore db = Firestore.instance;
 

    Future getCompanies() async {
    var companies = new List<CompanyModel>();
    QuerySnapshot query;

        query = await db
            .collection('company')
            .getDocuments();


    for (var item in query.documents) {
      companies.add(new CompanyModel(
          companyId: item.data['companyId'].toString(),
          name: item.data['name'].toString(),
          description: item.data['description'].toString(),
          email: item.data['email'].toString(),
          category: item.data['category'].toString(),
          address: item.data['address'].toString(),
          subcategory: item.data['subcategory'].toString(),
          phone: item.data['phone'].toString(),
          facebookLink: item.data['facebookLink'].toString(),
          mapLink: item.data['mapLink'].toString(),
          webLink: item.data['webLink'].toString(),
          profileImage: item.data['profileImage'].toString(),
          covers: item.data['cover'],
          tags: item.data['tag'],
          rating: double.parse(item.data['rating'].toString()),
          schedule: item.data['schedule'],
          isCompany: item.data['isCompany'],
          destacado: item.data['destacado']));
    }
    return companies;
  }

}