import 'package:cloud_firestore/cloud_firestore.dart';

class RatingService {
  final Firestore db = Firestore.instance;

  Future caculateRatingByCompany(String companyId) async {
    var query = await db
        .collection('rating')
        .where('companyId', isEqualTo: companyId)
        .getDocuments();

    double sum = 0.0;
    int count = 0;
    double prom = 0.0;
    if (query != null) {
      for (var item in query.documents) {
        sum = sum + double.parse(item.data['value'].toString());
      }
      count = query.documents.length;
      prom = sum / count;
    } else {
      return 0.0;
    }
    await db.collection('company').document(companyId).updateData({'rating': prom});
  }

  Future<double> getCompanyReview(String companyId, String userId) async {
    double rating;
    var query = await db
        .collection('rating')
        .where('companyId', isEqualTo: companyId)
        .where('userId', isEqualTo: userId)
        .getDocuments();

    if (query.documents.length != 0) {
      rating = double.parse(query.documents[0].data['value'].toString());
    } else {
      rating = 0.0;
    }

    return rating;
  }

  Future setCompanyReview(String companyId, String userId, double rating) async {

     var query = await db
        .collection('rating')
        .where('companyId', isEqualTo: companyId)
        .where('userId', isEqualTo: userId)
        .getDocuments();
  
    if (query.documents.length != 0) {
       var _currentDoc =  query.documents[0];
       await db.collection('rating').document(_currentDoc.documentID).updateData({'value': rating});
    } else {
       await db.collection('rating').add({'companyId': companyId, 'userId': userId, 'value': rating});
    }
  }
}
