import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:relapp_pruebas/model/model_categoria.dart';

class CategoryService {
  final Firestore db = Firestore.instance;

  Future getCategories() async {
    var categories = new List<CategoriaModel>();
    QuerySnapshot query;

    query = await db.collection('category').getDocuments();

    for (var item in query.documents) {
      categories.add(new CategoriaModel(
          nombreCategoria: item.data['name'].toString(),
          subcategorias: item.data['subcategory']));
    }
    return categories;
  }

  Future getCategoryByName(CategoriaModel category) async {
    await db
        .collection('category')
        .where('name', isEqualTo: category.nombreCategoria)
        .getDocuments()
        .then((query) {
      print(query.documents[0].data);
      if (query.documents.length != 0) {
        category.subcategorias = query.documents[0].data['subcategory'];
      }
    });
    return category;
  }
}
