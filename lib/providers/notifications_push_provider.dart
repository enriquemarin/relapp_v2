import 'dart:async';
import 'dart:io';
import 'package:firebase_messaging/firebase_messaging.dart';

class NotificationPushProvider{

  var _firebaseMessaging = FirebaseMessaging();
  final _streamController = StreamController<String>.broadcast();

  Stream<String> get notifications => _streamController.stream;

initNotifications (){
    _firebaseMessaging.requestNotificationPermissions();

    _firebaseMessaging.getToken().then((token){

      print('========TOKEN========');
      print(token);

    });

    _firebaseMessaging.configure(

      onMessage: (data) {
        print('======== On Message ===========');
        print(data);

        String noti = 'no-data';
        if(Platform.isAndroid){
          noti = data['data']['noti'] ?? 'no-data';
        }
        _streamController.sink.add(noti);
      },

      onLaunch: (data) {
        print('======== On Launch ===========');
        print(data);
      },

      onResume: (data) {
        print('======== On Resume ===========');
        print(data);

        final noti = data['data']['noti'];
        _streamController.sink.add(noti);
      }

    );

  }
  
  dispose(){
    _streamController.close();
  }

}